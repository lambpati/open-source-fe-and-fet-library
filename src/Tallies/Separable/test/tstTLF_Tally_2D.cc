
//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/FET/Manager_FET/Tallies/Separable/test/tstTLF_2D_Tally.cc
 * \brief  Track_Length_Flux_SFET class test
 */
//---------------------------------------------------------------------------//

#include <cmath>
#include <fstream>
#include <iostream>
#include <memory>
#include <random>

#include "FET_DBC.hh"

#include "Constants.hh"
#include "Domain.hh"

#include "Legendre_Attr.hh"
#include "Legendre_Poly.hh"

#include "Cosine_Attr.hh"
#include "Cosine_Poly.hh"
#include "Sine_Attr.hh"
#include "Sine_Poly.hh"

#include "Track_Length_S_FET.hh"

#include "Particle.hh"

namespace FET
{
    namespace TEST
    {
        //! \brief Constructs a TLF FET object with two dimension
        std::vector<Track_Length_Flux_SFET> TEST_TLF_2D()
        {
            // Make the domain
            const double bound = 1;
            const double shift = 0;
            const Domain<double> dom_x("x", -bound + shift, bound + shift);
//            const Domain<double> dom_x("x", -5, 2);
            const Domain<double> dom_y("y", -bound + shift, bound +  shift);
//            const Domain<double> dom_y("y", -5, 2);

//            const Domain<double> dom_2_x("x_2", -bound + shift, bound + shift);
//            const Domain<double> dom_2_y("y_2", -bound + shift, bound +  shift);

            // Create Attributes
            const Legendre_Attr  attr_x{"Legendre basis set in (x)", 8, dom_x};
            const Legendre_Attr  attr_y{"Legendre basis set in (y)", 8, dom_y};

//            const Legendre_Attr  attr_2_x{"Legendre basis set 2 in (x)", 5, dom_x};
//            const Legendre_Attr  attr_2_y{"Legendre basis set 2 in (y)", 5, dom_y};

            const Legendre_Poly<>  leg_poly_x{attr_x};
            const Legendre_Poly<>  leg_poly_y{attr_y};

//            const Legendre_Poly<>  leg_2_poly_x{attr_2_x};
//            const Legendre_Poly<>  leg_2_poly_y{attr_2_y};

            Track_Length_Flux_SFET FET_Legendre("Legendre-only TLF", {});
            FET_Legendre.add_basis_set(std::make_shared<Legendre_Poly<>>(leg_poly_x));
            FET_Legendre.add_basis_set(std::make_shared<Legendre_Poly<>>(leg_poly_y));

//            Track_Length_Flux_SFET FET_Legendre_2("Legendre-only TLF2", {});
//            FET_Legendre.add_basis_set(std::make_shared<Legendre_Poly<>>(leg_2_poly_x));
//            FET_Legendre.add_basis_set(std::make_shared<Legendre_Poly<>>(leg_2_poly_y));

            std::vector<Track_Length_Flux_SFET> fets(
                    {FET_Legendre}); //FET_Legendre_2});

            // Now test the various getters (also ensures proper construction)
            Insist(FET_Legendre.type() == "Track-length flux (separable FET)",
                   "Object type failed.");

            // Return the object for further testing
            return fets;
        }


        //! \brief Scores a 2-D TLF object
        void TEST_TLF_2D_SCORE(
                std::vector<Track_Length_Flux_SFET> fets)
        {

            const auto dom_x = fets[0].basis("x")->attr()->domain();
            const unsigned int num_bins = (int) 100;//5E2;

            const double bin_size_x = dom_x.range() / double(num_bins);

            const auto dom_y = fets[0].basis("y")->attr()->domain();
            //const unsigned int num_bins_y = (int) 5E2;
            const double bin_size_y = dom_y.range() / double(num_bins);

            // The sample function
            const double n = 1.0;
            const double m = 1.0;
            // Simple step functions: (n, x, x^2, etc.)
            // auto wgt_fct = [](double n, double x) { return n * x; };
            // auto wgt_fct = [](double n, double x) { return n * x * x; };
            // auto wgt_fct = [](double n, double x) { return n * x * x * x; };

            // Discontinuous functions
            /*
            auto wgt_fct = [](double n, double x) {
                if (x < -pi) { return 1.0; }
                if (x < 0.0) { return 2.0; }
                if (x < pi ) { return 1.0; }
                return 2.0 + 0 * n;
            };
            */
            /*
            auto wgt_fct = [](double n, double x) {
                return std::abs(n * pow(x, 3) - 6 * pow(x, 2) + 4 * x + 15); };
            */
            // Continuous functions
//            auto wgt_fct = [](double n, double x) {
//                const auto result = 0.5 + cos(n * x / (1.25 * pi));
//                if (result < 0 ) return 0.0;
//                return result; };

            auto wgt_fct = [](double n, double x, double m, double y) {
                const auto result = std::pow(x, 1.0);//1.0;//pow(y, 1.0);//1.0;//x * x * x * x;//cos(n*x)*cos(m*y);
//                if (result < 0) return 0.0;
                return result;
            };


            /*
             * Used for the ANS RPSD/ICRP 2020 paper on [0, 2pi]
             */
            /*
            auto wgt_fct = [](double n, double x) {
                const auto shift = 10.0;
                const auto scale = -0.50;
                const auto trans = pi;
                if (x >= trans)
                { return shift + sin (n * x); }
                // const auto a = - pow(n, 3) * cos(n * trans) / (6 * scale);
                const auto a = 1.0;
                const auto b = 3 * a * trans + pow(n, 2) * sin(n * trans) / (2 * scale);
                const auto c = n * cos(n * trans) / scale + 2 * b * trans -
                    3 * a * pow(trans, 2);
                const auto d = (sin(n * trans) + shift) / scale - a * pow(trans, 3) +
                    b * pow(trans, 2) - c * trans;
                return scale * (a * pow(x, 3) - b * pow(x, 2) + c * x + d);
            };
            */


            // Allocate memory
            std::vector<double> loc_x(int(num_bins), 0);
            std::vector<double> loc_y(int(num_bins), 0); // change variable x
            for (unsigned int i = 0; i < fets.size(); i++)
            {
                fets[i].reset();
            }

            // Now score
            const double inv_sqrt2 = 1 / std::sqrt(2.0);
//            const double half = 0.5;
            const std::vector<double> dir {inv_sqrt2, inv_sqrt2, 0.0};
            for (unsigned int i = 0; i < num_bins; i++)
            {
                // Get the location:
                double loc_of_x = dom_x.lower() + (bin_size_x * double(i));
                loc_x[i] = loc_of_x;// + bin_size_x / 2.0;

                for (unsigned int j = 0; j < num_bins; j++) {
                    double loc_of_y = dom_y.lower() + (bin_size_y * double(j));
                    loc_y[j] = loc_of_y;// + bin_size_y / 2.0; // Why does this change values drastically

                    // Create a particle at a point
                    Particle<> p;
                    p.set_direction(dir);
//                    p.set_position({loc_of_x, loc_of_y, 0});
                    p.set_position({loc_x[i], loc_y[j], 0});
                    p.set_step(std::sqrt(bin_size_x * bin_size_x + bin_size_y * bin_size_y));
                    p.set_weight(wgt_fct(n, loc_x[i], m, loc_y[j]));

                    // Score the particle now
                    for (unsigned int k = 0; k < fets.size(); k++) {
                        fets[k].score(p);
                    }
                }
            }

//             Finalize and evaluate:
            for (unsigned int i = 0; i < fets.size(); i++)
            {
                fets[i].finalize();
            }

            // Summarize file:
            std::ofstream file;
            file.open ("TLF_2D.txt");
            file << "Separable track length flux:\n"
                 << "2-D basis set in X on [" << dom_x.lower() << ", "
                 << dom_x.upper() << "]\n"
                 << num_bins << " data points exist.\n\n";

            file << "Separable track length flux:\n"
                 << "2-D basis set in Y on [" << dom_y.lower() << ", "
                 << dom_y.upper() << "]\n"
                 << num_bins << " data points exist.\n\n";

            // Print header
            const std::string delimeter = ",  ";
            file << "x_sim";

            file << delimeter << "y_sim";
            file << delimeter << "Actual";

            for (unsigned int i = 0; i < fets.size(); i++)
            {
                file << delimeter << fets[i].name();
            }

            file << "\n";

            // Now print evaluation data at each point scored
//            const auto normalization = (dom_x.range()*dom_y.range())/(num_bins*bin_size_x);// * bin_size_y);//-29.0;//(bin_size_x * bin_size_y) / (dom_x.range() * dom_y.range());//1.0 / (pow(num_bins, 2.0) * dom_x.range() * dom_y.range());//(bin_size_x * bin_size_y) / (dom_x.range() * dom_y.range());


            // Now print evaluation data at each point scored
//            const auto normalization = bin_size_x * bin_size_y / (dom_x.range() * dom_y.range());
//            for (unsigned int i = 0; i < num_bins; i++)
//            {
//                for (unsigned int j = 0; j < num_bins; j++)
//                {
//                    // Print domain values
//                    file << "\n" << loc_x[i] << delimeter << loc_y[j];
//
//                    // Print expected result
//                    file << delimeter << (wgt_fct(n, loc_x[i], m, loc_y[j]) * normalization);
//
//                    for (unsigned int k = 0; k < fets.size(); k++)
//                    {
//                        file << delimeter << fets[k].evaluate({loc_x[i], loc_y[j]});
//                    }
//                }
//            }
//            file.close();

            for (unsigned int i = 0; i < num_bins; i++)
            {
                for (unsigned int j = 0; j < num_bins; j++)
                {
                    file << loc_x[i] << delimeter << loc_y[j];

                    file << delimeter
                         <<  (wgt_fct(n, loc_x[i], m, loc_y[j]));// * normalization;
                    file << delimeter << (fets[0].evaluate({loc_x[i], loc_y[j]}));

                    file << "\n";
                }
            }

            file.close();

            // Compare data agains the "accepted" solution
            //! \todo Validate against accepted solution (regression test)

        }

        //! \brief Performs a 2-D test/scoring of the \c Track_Length_Flux_SFET
        void TEST_TLF_FET()
        {
            // Test construction and get object
            auto fets = TEST_TLF_2D();

            // Score and print results
             TEST_TLF_2D_SCORE(fets);
        }


    } // End of namespace TEST
} // End of namespace FET


int main()
{
    try {
        FET::TEST::TEST_TLF_FET();
    } catch (char const* message) {
        std::cerr << message << std::endl;
        return 1;
    }
    return 0;
}

//---------------------------------------------------------------------------//
// End of src/FET/Manager_FET/Tallies/Separable/test/tstTLF_Tally_2D.cc
//---------------------------------------------------------------------------//
