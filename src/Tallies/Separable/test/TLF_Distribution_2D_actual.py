# import pandas as pd
# import numpy as np
# import matplotlib
#
# matplotlib.use('TkAgg')
# import matplotlib.pyplot as plt
# import sys
# import csv
#
# from mpl_toolkits.mplot3d import axes3d
#
# # Read CSV
# csvFileName = sys.argv[1] if len(sys.argv) > 1 else '.'
#
# csvData = []
# with open('../../../../build/src/Tallies/Separable/test/TLF_2D.txt', 'r') as csvFile:
#     csvReader = csv.reader(csvFile, delimiter=',')
#     for num in range(0, 9):
#         next(csvReader)         #skip the first 9 lines of the file
#
#     for csvRow in csvReader:
#         csvData.append(csvRow)
#
# # Get X, Y, Z
# csvData = np.array(csvData)
# csvData = csvData.astype(np.double)
# X, Y, Z = csvData[:, 0], csvData[:, 1], csvData[:, 2]
#
# # Plot X,Y,Z
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.plot_trisurf(list(X), list(Y), list(Z), color='white', edgecolors='grey', alpha=0.5)
# ax.scatter(X, Y, Z, c='red')
# ax.set_xlabel("x")
# ax.set_ylabel("y")
# ax.set_title("Actual")
# plt.show()


import pandas as pd
import matplotlib.pyplot as plt

# data = pd.read_csv('../../../../build/src/Tallies/Separable/test/TLF_2D.txt', sep=',', skiprows=range(0, 8))

data = pd.read_csv('../../../../TLF_2D.txt', sep=',', skiprows=range(0, 9))

fig = plt.figure()
ax = plt.axes(projection='3d')

ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('FET')

data.columns = ['x', 'y', 'Actual', 'FET']
x = data['x']
y = data['y']
z = data['Actual']

ax.plot_trisurf(x, y, z, linewidth=0, antialiased=False)

# ax.scatter(x, y, z, color='red')

ax.set_title('Actual FET Surface')

plt.show()
plt.savefig('TLF_Actual.png', dpi=1200)
