import pandas as pd
import numpy as np
import matplotlib

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import sys
import csv
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cbook
from matplotlib import cm
from matplotlib.colors import LightSource
import matplotlib.pyplot as plt
import numpy as np
#import cairo

# filename = open('../../../../cmake-build-debug/TLF_2D.txt', 'r')
# with np.load(filename) as dem:
#     z = dem['elevation']
#     nrows, ncols = z.shape
#     x = np.linspace(dem['xmin'], dem['xmax'], ncols)
#     y = np.linspace(dem['ymin'], dem['ymax'], nrows)
#     x, y = np.meshgrid(x, y)
#
# region = np.s_[5:50, 5:50]
# x, y, z = x[region], y[region], z[region]
#
# fig, ax = plt.subplots(subplot_kw=dict(projection='3d'))
#
# ls = LightSource(270, 45)
# # To use a custom hillshading mode, override the built-in shading and pass
# # in the rgb colors of the shaded surface calculated from "shade".
# rgb = ls.shade(z, cmap=cm.gist_earth, vert_exag=0.1, blend_mode='soft')
# surf = ax.plot_surface(x, y, z, rstride=1, cstride=1, facecolors=rgb,
#                        linewidth=0, antialiased=False, shade=False)
#
# plt.show()

#Read CSV

csvFileName = sys.argv[1] if len(sys.argv) > 1 else '.'

csvData = []
with open('../../../../cmake-build-debug/src/Tallies/Separable/test/TLF_2D.txt', 'r') as csvFile:
    csvReader = csv.reader(csvFile, delimiter=',')
    for num in range(0, 9):
        next(csvReader)         #skip the first 9 lines of the file

    for csvRow in csvReader:
        csvData.append(csvRow)

# Get X, Y, Z
csvData = np.array(csvData)
csvData = csvData.astype(np.float)
X, Y, Z = csvData[:,0], csvData[:,1], csvData[:,3]

# Plot X,Y,Z
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_trisurf(X, Y, Z, color='white', edgecolors='grey', alpha=0.5)
ax.scatter(X, Y, Z, c='red')
ax.set_xlabel("x")
ax.set_ylabel("y")
ax.set_title("Estimated FET")
plt.show()
#
# import pandas as pd
# import numpy as np
# import matplotlib
#
# matplotlib.use('TkAgg')
# import matplotlib.pyplot as plt
# import sys
# import csv
# from mpl_toolkits.mplot3d import axes3d
# from matplotlib import cbook
# from matplotlib import cm
# from matplotlib.colors import LightSource
# import matplotlib.pyplot as plt
# import numpy as np
# plot = True
# if (plot):
#     print(f"Plotting enabled")
#     import matplotlib
#     import matplotlib.pyplot as plt
# else:
#     print(f"Plotting disabled.")
# # Create a dataframe containing the data in data
# data = pd.read_csv('../../../../cmake-build-debug/src/Tallies/Separable/test/TLF_2D.txt', sep=',', skiprows=range(0,8))
# print(f"Data columns: {data.columns}")
# # Plot the distribution of legendre polynomials
# if (plot):
#     fig = plt.figure()
#     ax = plt.axes(projection='3d')
# # Simple scale as needed (observe distribution)
# increment = 1
# for label in data.columns[3:]:
#     scalar = data['  Actual'].mean() / data[label].mean()
#     # Print scalar to terminal and graph
#     print("The {label} was being scaled by {scalar} for the following plot.")
#     if (plot):
#         ax.text(increment, increment, data['  Actual'].mean(),
#                 f"{label} scaled by {scalar}", color = "red")
#     # Now scale:
#     data[label] = data[label] * scalar
# # Specify data to plot
# print(f"{data.head(1000)}")
# if (plot):
#     for label in data.columns[3:]:
#         ax.plot_trisurf(data['x_sim'], data['  y_sim'], data["  Legendre-only TLF"],
#                         linewidth=0, antialiased=False)
#         # ax.plot_surface(data['x_sim'], data['y_sim'], data[label],
#         #                rstride = 1, cstride = 1,
#         #                cmap = 'viridis', edgecolor = 'none')
#     # Label the plot
#     ax.set_xlabel("X (units")
#     ax.set_ylabel("Y (units)")
#     ax.set_zlabel("Flux Value")
#     ax.set_title("2-D Track Length Flux FET Benchmark")
#     plt.legend(loc='best')
#     plt.grid()
#     plt.show()
#     plt.savefig('TLF_2D.png', dpi=1200)
#     plt.close(fig)
#
# import pandas as pd
# import matplotlib.pyplot as plt
#
# data = pd.read_csv('../../../../build/src/Tallies/Separable/test/TLF_2D.txt', sep=',', skiprows=range(0, 9))
#
# fig = plt.figure()
# ax = plt.axes(projection='3d')
#
# ax.set_xlabel('x')
# ax.set_ylabel('y')
# ax.set_zlabel('FET')
#
# data.columns = ['x', 'y', 'fet', 'Actual']
# x = data['x']
# y = data['y']
# z = data['fet']
#
# ax.plot_trisurf(x, y, z, linewidth=0, antialiased=False)
#
# # ax.scatter(x, y, z, color='red')
#
# ax.set_title('Estimated FET Surface')
#
# plt.show()
# plt.savefig('TLF_2D_Estimated.png', dpi=1200)
