//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Tallies/Base/test/tstBasis_Tally.cc
 * \brief  Basis_FET class test.
 */
//---------------------------------------------------------------------------//

#include <memory>

#include "FET_Macros.hh"
#include "FET_Assertions.hh"
#include "FET_DBC.hh"

#include "Domain.hh"
#include "Legendre_Attr.hh"
#include "Basis_Poly.hh"
#include "Legendre_Poly.hh"
#include "Basis_FET.hh"

#include "Particle.hh"

namespace FET
{
namespace TEST
{
    //! \brief Tests construction of the \c Basis_FET object
    Basis_FET TEST_Basis_FET_Construction()
    {
        // Make some domains
        const Domain<double> dom1{"x", -10.0,  0.0};
        const Domain<double> dom2{"y",   0.0, 10.0};
        const Domain<double> dom3{"z", -10.0, 10.0};
        const Domain<double> dom4{"unsupported domain", 0.0, 100.0};

        // Create attributes
        const Legendre_Attr attr1{"x basis set",      5, dom1};
        const Legendre_Attr attr2{"y basis set",     18, dom2};
        const Legendre_Attr attr3{"z basis set",      0, dom3};
        const Legendre_Attr attr4{"other basis set", 12, dom4};

        // Create basis sets
        const Legendre_Poly<> l_poly1{attr1};
        const Legendre_Poly<> l_poly2{attr2};
        const Legendre_Poly<> l_poly3{attr3};
        const Legendre_Poly<> l_poly4{attr4};

        // Create the vector
        std::vector<std::shared_ptr<Basis_Poly<>>> basis_sets(5);
        basis_sets[0] = std::make_shared<Legendre_Poly<>>(l_poly1);
        basis_sets[1] = std::make_shared<Legendre_Poly<>>(l_poly2);
        basis_sets[2] = std::make_shared<Legendre_Poly<>>(l_poly3);
        // Inclusion of this should throw an error (repeated)
        basis_sets[3] = std::make_shared<Legendre_Poly<>>(l_poly1);
        // Inclusion of this should throw an error (invalid)
        basis_sets[4] = std::make_shared<Legendre_Poly<>>(l_poly4);

        // Construct the FET improperly (test \c validate_basis_set)
        for (int i = 0; i < 2; i++)
        {
            try
            {
                Basis_FET fet{"Some FET", basis_sets};
                throw "Use of unsupported domains did NOT fail!";
            } catch (assertion& e) {
                // Behaves fine
            }

            // Remove the last element
            basis_sets.pop_back();
        }

        // Construct properly now
        Basis_FET fet{"Some FET", basis_sets};
        fet.set_description("An FET of some sorts");

        // Now test the various getters (also ensures proper construction)
        Insist(fet.type() == "Basis FET", "Object type failed.");
        Insist(fet.name() == "Some FET", "The name was not properly set");
        Insist(fet.description() == "An FET of some sorts",
                "The description setter/getter failed.");
        Insist(fet.basis(0)->attr()->name() == l_poly1.attr()->name(),
                "The integer 'basis' method failed.");
        Insist(fet.basis("z")->attr()->name() == l_poly3.attr()->name(),
                "The string 'basis' method failed.");
        Insist(fet.domain_exists("x"),
                "An existing domain was NOT found.");
        Insist(!fet.domain_exists("other"),
                "A non-existant domain was found.");
        Insist(fet.contains({0.0, 0.0, 0.0}),
                "An location in the domain wasn't found.");
        Insist(!fet.contains({-100.0, -100.0, -100.0}),
                "A location outside the domain was found to be inside "
                << "the domain.");

        // Test that the basis sets are non-basis types (i.e. have an
        // evaluate implementation)
        try {
            const auto dummy = fet.basis("x")->evaluate(0.5);
            FET_IGNORE(dummy);
        } catch (assertion& e) {
            throw "The FET did not load the proper basis set";
        }


        // Return the object for further testing
        return fet;
    }


    //! \brief Tests all \c Not_Implemented features of the \c Basis_FET
    void TEST_Basis_FET_Not_Implemented(Basis_FET& fet)
    {
        // For 'reset'
        try{
            fet.reset();
        } catch (not_implemented_error& e) {
            // Works fine
        }

        // For 'evaluate'
        try{
            const auto dummy = fet.evaluate({0.0, 1.0, 0.0});
            FET_IGNORE(dummy);
        } catch (not_implemented_error& e) {
            // Works fine
        }

        // For 'variance'
        try{
            const auto dummy = fet.variance({0.0, 1.0, 0.0});
            FET_IGNORE(dummy);
        } catch (not_implemented_error& e) {
            // Works fine
        }

        // For 'score'
        try{
            const auto p = Particle<>();
            fet.score(p);
        } catch (not_implemented_error& e) {
            // Works fine
        }

        // For 'finalize'
        try{
            fet.finalize(100);
        } catch (not_implemented_error& e) {
            // Works fine
        }
    }

    //! \brief Performs all test of the \c Basis_FET API and implementation
    void TEST_Basis_FET()
    {
        // Test construction and get object
        auto fet = TEST_Basis_FET_Construction();

        // Test features NOT implemented
        TEST_Basis_FET_Not_Implemented(fet);
    }


} // End of namespace TEST
} // End of namespace FET


int main()
{
    try {
        FET::TEST::TEST_Basis_FET();
    } catch (char const* message) {
        std::cerr << message << std::endl;
        return 1;
    }
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Tallies/Base/test/tstBasis_Tally.cc
//---------------------------------------------------------------------------//
