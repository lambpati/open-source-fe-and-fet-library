//---------------------------------------------------------------------------//
/*!
 * \file  src/Tallies/Basis_FET.hh
 * \brief Basis_FET class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Tallies_Base_Basis_FET_hh
#define src_Tallies_Base_Basis_FET_hh

#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>
#include <string>

#include "FET_Macros.hh"
#include "FET_DBC.hh"
#include "Basis_Poly.hh"
#include "Particle.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Basis_FET
 * \brief General structure and API of FET objects
 *
 * The Basis_FET object acts as the general API for each of the FET
 * objects. These objects may be used for estimating particle flux,
 * particle current, and potentially many other types of tallies.
 */
/*!
 * \example src/Tallies/Base/test/tstBasis_FET.cc
 *
 * Test of the Basis_FET object
 */
//===========================================================================//
    class Basis_FET
    {
    public:

        //@{
        //! Public type aliases
        using SP_Basis_Poly       = std::shared_ptr<const Basis_Poly<>>;
        using Vec_SP_Basis_Poly   = std::vector<SP_Basis_Poly>;
        using Vec_Dbl             = std::vector<double>;
        using Vec_Vec_Dbl         = std::vector<Vec_Dbl>;
        //@}

        //! \breif Vector of basis sets for the FET
        Vec_SP_Basis_Poly d_basis;

    private:
        // >>> IMPLEMENTATION DATA

        //! \brief Name of the FET object
        std::string b_name = "";

        //! \brief Description of the FET object
        std::string b_description;

        //! \brief Vector of the basis set domain labels
        //!
        //! A quick-reference for the domain name of the basis sets used. This
        //! allows for using the std::find algorithm easily to search for a basis
        //! set by it's domain name.
        std::vector<std::string> d_domain;

    protected:
        //! \brief The number of scored particles
        unsigned int d_num_scores = 0;


    public:
        // >>> CONSTRUCTOR/DESTRUCTORS

        //! \brief Basic Constructor
        Basis_FET(const std::string &name,
                  const std::vector<std::shared_ptr<Basis_Poly<>>> &basis_sets)
                : b_name("untitled")
                ,  d_num_scores(0)
        {
            // Set name
            set_name(name);

            // Add the basis sets
            add_basis_sets(basis_sets);
        }

        //! \brief Constructor (in invalid state)
        Basis_FET()
                : b_name("untitled")
                , d_num_scores(0)
        { /* * */ }

        //! Destructor
        virtual ~Basis_FET() = default;


    public:
        // >>> SETTERS

        //! \brief Set label/name of the FET
        void set_name(const std::string &name)
        {
            Insist(!name.empty(),
                   "An invalid name was given to the " << type() << " object.");
            b_name = name;
        };

        //! \brief Set the description of the object
        void set_description(const std::string &description) noexcept
        {
            b_description = description;
        }

        //! \brief Resets the coefficients and associated variances
        virtual void reset()
        { Not_Implemented("reset"); }

    //! \brief Validates that a basis set is supported and does not exist yet
    void validate_basis_set(const SP_Basis_Poly basis_set) const
    {
        // Require that domain be valid by FET usage
        Insist (basis_set->attr()->domain().label() == "x" ||
                basis_set->attr()->domain().label() == "y" ||
                basis_set->attr()->domain().label() == "z" ||
                basis_set->attr()->domain().label() == "r" ||
                basis_set->attr()->domain().label() == "theta" ||
                basis_set->attr()->domain().label() == "phi",
                "An basis set on the " << basis_set->attr()->domain().label()
                    << " domain is not supported within FETs.");

        // Ensure the domain does NOT already exist
        Insist (!domain_exists(basis_set->attr()->domain().label()),
                "The " << basis_set->attr()->domain().label()
                    << " domain cannot be duplicated within the FET.");
    }

    //! \brief Adds a vector of basis sets to the FET object
    //! \note This method is \c private so that clients cannot add more basis
    //! sets after having created the coefficient arrays.
    void add_basis_sets(const std::vector<std::shared_ptr<Basis_Poly<>>> &basis_sets)
    {
        // Add all non-null pointing shared_ptrs to the basis set vector
        for (unsigned int i = 0; i < basis_sets.size(); i++)
        {
            if (!(basis_sets[i] == nullptr )) add_basis_set(basis_sets[i]);
        }
    }

    //! \brief Adds a basis set to the FET
    //! \note This method is \c private so that clients cannot add more basis
    //! sets after having created the coefficient arrays.
    virtual void add_basis_set(SP_Basis_Poly basis_set)
    {
        // Validate the basis set
        validate_basis_set(basis_set);

        // Add the basis set
        d_basis.push_back(basis_set);

        // Add the domain label quick reference
        d_domain.push_back(basis_set->attr()->domain().label());
    }


    public:
        // >>> GETTTERS

        virtual Vec_Vec_Dbl coefficients() const
        {
            Vec_Dbl v = {1,2,3};
            Vec_Vec_Dbl v2 = {v,v,v};
            return v2;
        }
        virtual Vec_Vec_Dbl variance() const
        {
            Vec_Dbl v = {1,2,3};
            Vec_Vec_Dbl v2 = {v,v,v};
            return v2;
        }
        virtual Vec_Dbl flux() const
        {
            Vec_Dbl v = {1,2,3};
            return v;
        }

        virtual Vec_Vec_Dbl locations() const
        {
            Vec_Dbl v = {1,2,3};
            Vec_Vec_Dbl v2 = {v,v,v};
            return v2;
        }

        virtual Vec_Dbl energies() const
        {
            Vec_Dbl v = {1,2,3};
            return v;

        }


        //! \brief Returns the FET object type
        virtual const std::string type() const noexcept
        { return "Basis FET"; }

        //! \brief Returns the name/label of the FET object
        const std::string name() const noexcept
        { return b_name; }

        //! \brief Returns the description of the FET object
        const std::string description() const noexcept
        { return b_description; }

        //! \brief Returns the vector of basis sets
        Vec_SP_Basis_Poly basis() const noexcept
        { return d_basis; }

        SP_Basis_Poly basis(int index) const noexcept
        { return d_basis[index]; }

    //! \brief Returns the basis set with the desired domain label
    SP_Basis_Poly basis(const std::string &label) const
    {
        // Ensure the label exists in the FET
        Require(domain_exists(label));

        // Obtain index of the basis set
        const auto label_it = std::find(d_domain.begin(), d_domain.end(), label);
        const auto index = unsigned(int(
                    std::distance(d_domain.begin(), label_it)));

        // Return the basis set
        return basis(index);
    }

        //! \brief Returns if a basis set on a domain exists within the FET already
        bool domain_exists(const std::string &label) const noexcept
        { return
                    std::find(d_domain.begin(), d_domain.end(), label) != d_domain.end();
        }

        //! \brief Returns the number of particles scored
        unsigned int num_scored() const noexcept
        { return d_num_scores; }


    public:
        // >>> EVALUATION

        //! \brief Returns the value of the FET at a single location
        virtual double evaluate(const Vec_Dbl &location)
        {
            // Throw an error
            Not_Implemented("evaluate");

            // Use variables to avoid DEBUG compiler errors
            FET_IGNORE(location);
            return 0.0;

        }

        //! \brief Returns the variance of the coefficients at a point
        virtual double variance(const Vec_Dbl &location) const
        {
            // Throw an error
            Not_Implemented("variance");

            // Use variables to avoid DEBUG compiler errors
            FET_IGNORE(location);
            return 0.0;
        }

        virtual void evaluate_flux(const Particle<>& p){

            Not_Implemented("flux");

            FET_IGNORE(p);
         }


    public:
        // >>> INTROSPECTION

        //! \brief Describes the object
        //! \todo Implement 'describe' for FET objects
        virtual void describe(std::ostream& os) const
        {
            os << "Basis_FET::describe(std::ostream& os) not implemented\n";
            // Throw an error
            Not_Implemented("describe");
        }

    //! \brief Returns if a point is within the domain of the FET
    bool contains(const Vec_Dbl &location) const
    {
        // Ensure the size is that of the FET
        Require(location.size() == basis().size());

        // Iterate over each domain and verify if within
        for (unsigned int i = 0; i < location.size(); i++)
        {
            if (!basis(i)->attr()->domain().contains(location[i]))
            {
                return false;
            }
        }
        return true;
    }

        //! \brief Scores a particle
        virtual void score(const Particle<>& p)
        {
            // Throw an error
            Not_Implemented("score");

            // Use the variable to ignore DEBUG errors
            FET_IGNORE(p);
        }

        //! \brief Returns the volume of the FET
        virtual double volume() const
        {
            if (basis().size() == 0) return 0.00;

            double vol = 1.00;
            for (unsigned int i = 0; i < basis().size(); i++)
            {
                vol *= basis(i)->attr()->domain().range();
            }
            return vol;
        }

        //! \brief Finalizes the coefficient and variance sets
        virtual void finalize(const double divisor = 1.0)
        {
            // Throw an error
            Not_Implemented("score");

            // Use the variable to ignore DEBUG errors
            FET_IGNORE(divisor);
        }

        //! \brief Normalizes the coefficients and variances by some value
        virtual void normalize(const double divisor = 1.0)
        {
            // Throw an error
            Not_Implemented("normalize");

            // Use the variable to ignore DEBUG errors
            FET_IGNORE(divisor);
        }

    }; // end class Basis_FET

    //---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_Tallies_Base_Basis_FET_hh
//---------------------------------------------------------------------------//
// end of src/Tallies/Base/Basis_FET.hh
//---------------------------------------------------------------------------//
