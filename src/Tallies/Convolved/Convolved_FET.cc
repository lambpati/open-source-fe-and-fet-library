//---------------------------------------------------------------------------//
/*!
 * \file  src/FET/Manager_FET/Tallies/Convolved_FET.cc
 * \brief Results_Convolved class declaration.
 */
//---------------------------------------------------------------------------//

#include <iostream>

#include "Results_Convolved.hh"

namespace FET
{

//---------------------------------------------------------------------------//
/*!
 * \brief Particle Constructor
 */
Particle::Particle()
    : d_position({0.0, 0.0, 0.0})
    , d_dir({1.0, 0.0, 0.0})
    , d_step(0.0)
    , d_weight(1.0)
    , d_energy(14.0)
    , d_total_cross_section(0.0)
{
    /* * */
}

//---------------------------------------------------------------------------//
/*!
 * \brief Particle Destructor
 */
Particle::~Particle() = default;



//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// end of src/FET/Manager_FET/Tallies/Convolved_FET.cc
//---------------------------------------------------------------------------//
