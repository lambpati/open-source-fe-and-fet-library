//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/core/Site_Handler.hh
 * \brief  Site_Handler class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_core_Site_Handler_hh
#define src_core_Site_Handler_hh

#include <vector>

#include "FET_DBC.hh"

namespace FET
{

/*!
 * \namespace Geometry
 * \brief Provides simple geometric manipulations
 *
 * Provides geometric manipulations and objects. Presently supports cartesian
 * transformations to cylindrical and spherical geometries, supporting 2-D and
 * 3-D geometries.
 *
 * Further provides objects that can transform from one standard geometry to
 * another, taking vector-based indice data and transforming it to the desired
 * geometry data using the provided transformations.
 */
namespace Geometry
{

//===========================================================================//
/*!
 * \brief Enumerates the geometries supported by the \c Site_Handler object.
 *
 * The \c cartestian  state identifies the geometry as (x, y, z).
 * The \c cylindrical state identifies the geometry as (r, theta, x/y/z).
 * The \c spherical   state identifies the geometry as (r, theta, phi).
 *
 * Other supported geometry types ought to be added here as needed by
 * consumers.
 */
enum class geometry
{
    cartesian = 0,
    cylindrical,
    spherical
};


//===========================================================================//
/*!
 * \class Site_Handler
 * \brief Manipulates site (location) data to that of another coordinate system
 *
 * This object allows its consumer to provide it with position data, such as
 * Cartesian, cylindrical, or spherical, and the object will manipulate, or
 * transofrm, the data to that required by the consumer.
 *
 * For example, the consumer can do so as follows:
 * \code

   Site_Handler<>(CARTESIAN, CYLINDRICAL) transformer;
   auto location = std::vector<double>{1.0, 1.0, 1.0};

   // Extract r, theta
   auto r = transformer.extract_r(location);
   auto theta = transformer.extract_theta(location);

 * \endcode
 *
 * The dimensionality of the provided data must be at least that of the client.
 * Therefore, clients must ensure correct usage of this object.
 *
 * \todo This has not been completed or analyzed further.
 */
/*!
 * \example test/tstSite_Handler.cc
 *
 * Tests the Site_Handler object
 *
 */
//===========================================================================//
template<typename T = double, class U = std::vector<T>>
class Site_Handler
{
  public:
    //@{
    //! Public type aliases
    using string         = std::string;
    using number_type    = unsigned int;
    using geometry_type  = geometry;
    //@}


  private:
    // >>> IMPLEMENTATION DATA

    //! \brief The required dimensionality of the client
    number_type d_dimension = 1;

    //! \brief Dimensionality of the provided data
    geometry_type d_from = geometry_type::cartesian;

    //! \brief Dimensionality of the consumer
    geometry_type d_to = geometry_type::cartesian;


  public:
    // >>> CONSTRUCTION

    //! \brief Simple constructor using both arguments
    Site_Handler()
    {
    }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Site_Handler(const Site_Handler&) = default;
    Site_Handler& operator=(const Site_Handler&) = default;
    Site_Handler(Site_Handler&&) = default;
    Site_Handler& operator=(Site_Handler&&) = default;
    virtual ~Site_Handler() = default;
    //@}


  public:
    // >>> SETTERS


  public:
    // >>> GETTERS


  public:
    // >>> INSTROSPECTION


}; // end class Site_Handler

//---------------------------------------------------------------------------//
} // end namespace Geometry
} // end namespace FET

#endif // src_core_Site_Handler_hh
//---------------------------------------------------------------------------//
// end of src/core/Site_Handler.hh
//---------------------------------------------------------------------------//
