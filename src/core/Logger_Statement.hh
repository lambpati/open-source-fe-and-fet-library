//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/core/Logger_Statement.hh
 * \author Seth R Johnson
 * \date   Sat Feb 21 00:12:15 2015
 * \brief  Logger_Statement class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_core_Logger_Statement_hh
#define src_core_Logger_Statement_hh

#include <iostream>
#include <memory>
#include <sstream>
#include <vector>

#include "FET_DBC.hh"

namespace FET
{

//===========================================================================//
/*!
 * \class Logger_Statement
 * \brief Support class for Logger that emulates an ostream.
 *
 * This class is designed to intercept ostream-like output, save it to a
 * buffer, and send it to multiple streams when it reaches the end of its
 * scope.
 *
 * It should never be stored; its lifetime should be the the scope of the
 * single statement from which it's created.
 *
 * The vector of string arguments are "reference"-like pointers: they should be
 * temporary (lifespan of the logger statement) and will not be deleted.
 */
//===========================================================================//
class Logger_Statement
{
    typedef Logger_Statement This;
  public:
    // >>> PUBLIC TYPE ALIASES

    //! Output string type
    using ostream_t = std::ostream;

    //! Vector of pointers to output streams
    using Vec_Ostream = std::vector<ostream_t*>;

    //! Function signature for a stream maniupulator (such as endl)
    using Stream_Manipulator = ostream_t& (*)(ostream_t&);


  private:
    // >>> IMPLEMENTATION DATA

    //! String stream type compatible with ostream type
    using osstream_t = std::basic_ostringstream<ostream_t::char_type,
                                                ostream_t::traits_type>;

    //! String stream for saving this log message
    std::unique_ptr<osstream_t> d_message;

    //! Vector of "sinks" to output to
    Vec_Ostream d_sinks;


  public:
    // >>> CONSTRUCTORS/DESTRUCTORS

    // Construct with streams to send message to
    explicit Logger_Statement(Vec_Ostream streams)
    : d_sinks(std::move(streams))
    {
        // Require streams to point somewhere
    #if defined(REQUIRE_ON)
        for (auto sink : d_sinks)
        {
            Require(sink);
        }
    #endif

        if (!d_sinks.empty())
        {
            // Allocate a message stream if we're actually doing output
            d_message.reset(new osstream_t());
        }
        Ensure(!d_sinks.empty() == static_cast<bool>(d_message));
    }

    // Send message on destruction
    ~Logger_Statement()
    { flush(); }

    // Allow moving but not copying
    Logger_Statement(Logger_Statement&&) = default;
    Logger_Statement& operator=(Logger_Statement&&) = default;
    Logger_Statement(const Logger_Statement&) = delete;
    Logger_Statement& operator=(const Logger_Statement&) = delete;


  public:
    // >>> INTROSPECTION

    //! \brief Flushes all stored messages
    void flush()
    {
        if (!d_message)
            return;

        try
        {
            // Add a trailing newline
            *d_message << "\n";

            // Get the string output
            const auto& message = d_message->str();

            // Write it to all the streams
            for (auto* stream_ptr : d_sinks)
            {
                *stream_ptr << message << std::flush;
            }
        }
        catch (const std::exception& e)
        {
            std::cerr << "An error occurred writing a log message: "
                      << e.what()
                      << std::endl;
        }
    }

    /*!
     * \brief Act like an ostream, but return ourself.
     *
     * This allows us to intelligently disable writing expensive operations to
     * the stream if they're not going to be output. If we're saving output,
     * write the given data to the string stream.
     */
    template<class T>
    This& operator<<(const T& rhs)
    {
        if (d_message)
        {
            *d_message << rhs;
        }
        return *this;
    }

    /*!
     * \brief Specialization on const char* to reduce object size.
     */
    This& operator<<(const char* rhs)
    {
        if (d_message)
        {
            *d_message << rhs;
        }
        return *this;
    }

    /*!
     * \brief Accept manipulators such as std::endl.
     *
     * This allows us to intelligently disable writing expensive operations to
     * the stream if they're not going to be output.
     */
    This& operator<<(Stream_Manipulator manip)
    {
        if (d_message)
        {
            manip(*d_message);
        }
        return *this;
    }


};

//---------------------------------------------------------------------------//
} // end namespace nemesis

//---------------------------------------------------------------------------//
#endif // src_core_Logger_Statement_hh

//---------------------------------------------------------------------------//
// end of src/core/Logger_Statement.hh
//---------------------------------------------------------------------------//
