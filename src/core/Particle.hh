//---------------------------------------------------------------------------//
/*!
 * \file  src/core/Particle.hh
 * \brief Particle class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_core_Particle_hh
#define src_core_Particle_hh

#include <cmath>
#include <iostream>
#include <vector>

#include "Constants.hh"
#include "FET_DBC.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Particle
 * \brief A simple particle core for testing the FET library
 *
 * The Particle class is used by the FET library as a core to allow testing
 * of the library. The library is developed using the \c template<> method. The
 * Particle that software clients use with the FET library needs to have
 * \c getter functions as described here, however internal implementation may be
 * specified other than that given here.
 *
 * The Particle object uses virtualization to allow software clients to
 * derive their own Particle object from this one, if desired.
 */
/*!
 * \example src/core/test/tstParticle.cc
 *
 * Test of the Particle object
 */
//===========================================================================//
template<typename T = double>
class Particle
{
public:
    // >>> PUBLIC ALIASES

    //@{
    //! Public aliases
    using type              = T;
    using Vec_Type          = std::vector<type>;
    //@}


private:
    //! >>> IMPLEMENTATION DATA

    //! \brief States whether particle is active or not
    bool d_alive = true;

    //! \brief Initial position of the particle
    Vec_Type d_position = {0, 0, 0};

    /*!
     * \brief Direction of the particle
     *
     * Direction of the particle, as follows:
     * \f[
     * dir[0] = sin\theta * cos\phi
     * dir[1] = sin\theta * sin\phi
     * dir[2] = cos\theta
     * \f]
     */
    Vec_Type d_dir = {1, 1, 1};

    //! \brief Distance traveled (path length) since last collision
    type d_step = 0.0;

    //! \brief Weight of the particle
    type d_weight = 1.0;

    //! \brief Energy of the particle, in MeV
    type d_energy = 14.0;

    //! \brief The probability of two particles scattering from each other
    //! due to their interaction
    type d_total_cross_section = 0.0;


public:
    //! >>> CONSTRUCTORS

    //@{
    //! Copy control (default, copy, move, and destructor)
    Particle() = default;
    Particle(const Particle&) = default;
    Particle& operator=(const Particle&) = default;
    Particle(Particle&&) = default;
    Particle& operator=(Particle&&) = default;
    virtual ~Particle() = default;
    //@}


public:
    //! >>> SETTERS

    //! Sets whether the particle is alive or not
    void set_alive(const bool alive) noexcept
    { d_alive = alive; }

    //! Sets the position of the particle
    virtual void set_position(const Vec_Type& position)
    {
        Require(position.size() == 3);
        d_position = position;
    }

    //! Updates the position of the particle given its direction and step
    virtual void update_position()
    { set_position(next_position()); }

    //! Sets the direction of the particle
    virtual void set_direction(const Vec_Type& dir);

    //! Sets the distance traveled since the last collision
    virtual void set_step(const type step)
    {
        Require(step >= 0);
        d_step = step;
    }

    //! Sets the weight of the particle
    virtual void set_weight(const type weight)
    {
//        Require(weight >= 0);
        d_weight = weight;
    }

    //! Sets the energy of the particle
    virtual void set_energy(const type energy)
    {
        Require(energy >= 0);
        d_energy = energy;
    }

    //! Sets the probability of two particles scattering from each other
    //! due to their interaction
    virtual void set_total_cross_section(const type total_cross_section)
    {
        Require(total_cross_section > 0.0);
        d_total_cross_section = total_cross_section;
    }


public:

    //! >>> INTROSPECTION/GETTTERS

    //! Returns if the particle is alive or not
    bool alive() const noexcept
    { return d_alive; }

    //! Returns the initial position of the particle
    Vec_Type position() const noexcept
    { return d_position; }

    //! Returns the future position of the particle
    virtual Vec_Type next_position() const noexcept;

    //! Returns the direction of the particle
    Vec_Type dir() const noexcept
    { return d_dir; }

    //! Returns the distance traveled since the last collision
    type step() const noexcept
    { return d_step; }

    //! Returns the weight of the particle
    type weight() const noexcept
    { return d_weight; }

    //! Returns the energy of the particle
    type energy() const noexcept
    { return d_energy; }

    //! Returns the probability of two particles scattering from each other
    //! due to their interaction
    type total_cross_section() const noexcept
    { return d_total_cross_section; }

    //! \brief Describes the Particle object
    virtual void describe(std::ostream os) const noexcept;


}; // end class Particle

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// INLINE AND TEMPLATE DEFINITIONS
//---------------------------------------------------------------------------//

#include "Particle.i.hh"

//---------------------------------------------------------------------------//
#endif // src_core_Particle_hh
//---------------------------------------------------------------------------//
// end of src/core/Particle.hh
//---------------------------------------------------------------------------//
