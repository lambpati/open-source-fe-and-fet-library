//---------------------------------------------------------------------------//
/*!
 * \file  src/core/Particle.i.hh
 * \brief Particle class template definitions.
 */
//---------------------------------------------------------------------------//
#ifndef src_core_Particle_i_hh
#define src_core_Particle_i_hh

#include <iostream>
#include <vector>

#include "Particle.hh"
#include "Constants.hh"
#include "FET_DBC.hh"

namespace FET
{

//---------------------------------------------------------------------------//
/*!
 * \brief Sets the direction of the particle
 */
template<typename T>
void Particle<T>::set_direction(const Particle<T>::Vec_Type& dir)
{
    Require(dir.size() == 3);
    //Require((std::pow(dir[0],2) + std::pow(dir[1],2) + std::pow(dir[3],2))
    //        - 1.0 <= tolerance);
    d_dir = dir;
}


//---------------------------------------------------------------------------//
/*!
 * \brief Returns the future position of the particle
 */
template<typename T>
std::vector<T> Particle<T>::next_position() const noexcept
{
    Vec_Type next_pos(3);
    for(unsigned int i = 0; i < position().size(); ++i)
    {
        next_pos[i] = position()[i] + step() * dir()[i];
    }
    return next_pos;
}


//---------------------------------------------------------------------------//
/*!
 * \brief Descibes the Particle object
 *
 * Writes a description of the object to a given output stream in a Markdown
 * format.
 */
template<typename T>
void Particle<T>::describe(std::ostream os) const noexcept
{
    // Obtain quick refernce to object data
    const auto p_pos = position();
    const auto p_dir = dir();

    // Describe the object
    os << "This particle is as follows:\n"
       << "+ Alive = " << alive() << "\n"
       << "+ Position (cm) = [" << p_pos[0] << ", "
          << p_pos[1] << ", " << p_pos[2] << "]\n"
       << "+ Normalized direction = [" << p_dir[0] << ", "
          << p_dir[1] << ", " << p_dir[2] << "]\n"
       << "+ Step size = " << step() << " cm\n"
       << "+ Energy = " << energy() << " MeV\n"
       << "+ Total cross section = " << total_cross_section() << " b\n"
       << "+ Weight = " << weight() << "\n";
}


//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_core_Particle_i_hh
//---------------------------------------------------------------------------//
// end of src/core/Particle.i.hh
//---------------------------------------------------------------------------//
