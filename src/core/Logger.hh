//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   FET/comm/Logger.hh
 * \author Seth R Johnson
 * \date   Tue Feb 03 08:39:27 2015
 * \brief  Logger class declaration.
 */
//---------------------------------------------------------------------------//

#ifndef src_core_Logger_hh
#define src_core_Logger_hh

// Workaround for Windows
#ifdef ERROR
#undef ERROR
#endif

// Workaround for LibMesh, because it will # define DEBUG.
#ifdef DEBUG
#undef DEBUG
#endif

#include <memory>
#include <iostream>

#include "Logger_Statement.hh"
#include "FET_DBC.hh"

//---------------------------------------------------------------------------//
// ANONYMOUS HELPER FUNCTIONS
//---------------------------------------------------------------------------//
/*!
 * \brief Allows any namespace to use the \c Logger easily
 */
namespace
{
//! \brief Custom deleter so that a regular pointer can be wrapped in an SP
struct null_deleter
{
    void operator()(const void*)
    { /* * */ }
};
} // end anonymous namespace

namespace FET
{

//============================================================================//
/*!
 * \page Logging Logging messages within the FET library
 *
 * DEBUG messages are primarily for useful fine-grained diagnostics.
 *
 * STATUS messages should be essentially the same at a given program point
 * regardless of the problem, e.g. "Building FETs"
 *
 * INFO messages contain problem-specific information, e.g. "Legendre
 * polynomial made with order 10"
 *
 * WARNING messages should be about unexpected or unusual behavior, e.g. "The
 * FET contains the 'x' domain already."
 *
 * ERROR messages are those that where a recovery attempt is being made.
 *
 * CRITICAL messages are those associated with fatal errors and unresolvable
 * error messages.
 *
 * Messages are logged according to the specified level, where lower
 * level messages may be omitted.
 *
 * Example messages:
 * \code

   log(DEBUG) << "Constructing with " << type << " polynomial.";
   log_local(DIAGNOSTIC) << "Finished scoring particle << i << ".";
   log(STATUS) << "Building managers...";
   log(INFO) << "Built the " << n << "-dimensional coefficient matrix.";
   log(WARNING) << "The " << type << " polynomial was truncated to order "
                << order << ".";
   log_local(ERROR) << "Out of bounds error for coefficient order " << n;
   log(CRITICAL) << "Caught exception " << e.what() << "; aborting.";

 * \endcode
 *
 * \note This implementation is directly copied from that of the Shift team at
 * ORNL, with slight modification made. Credit for this is given to the
 * Shift team.
 *
 */
//============================================================================//


//============================================================================//
/*!
 * \brief The level at which messages are logged
 *
 * The \c Log_Level specifies which messages are logged and which aren't
 *
 * Available \c Log_Level's include DEBUG, DIAGNOSTIC, INFO, STATUS, WARNING,
 * ERROR, and CRITICAL. Not all of these levels are unique at present, but
 * provide future extensibility and ease of use.
 */
enum Log_Level
{
    DEBUG = 0,   //!< General debugging messages
    DIAGNOSTIC,  //!< See \c DEBUG
    STATUS,      //!< Current program execution messages (related to \c INFO)
    INFO,        //!< Important informational messages regarding the problem
    WARNING,     //!< Warnings about unusual or unexpected events
    ERROR,       //!< Something went wrong, but execution may continue
    CRITICAL,    //!< Something went terribly wrong, execution halts.
    END_LOG_LEVEL //!< Aid to properly size \c log_prefix's
};

/*!
 * \class Logger
 * \brief Global parallel logging for FET.
 *
 * This singleton class is generally accessed via the "log" free function.
 *
 * Currently the node ID is saved whenever the logger is instantiated (first
 * called), so if the communicator is changed, the original "master" node will
 * be the only one that logs during a global call.
 *
 * The class is designed to replace: \code

    if (node() == 0)
    {
        cout << ">>> Global message" << endl;
    }

    cout << ">>> Encountered " << n << " cowboys on node " << node() << endl;
    \endcode

 * with \code

    FET::log() << "Global message" ;
    FET::log_local() << "Encountered " << n << " cowboys on node "
                         << node() ;
 * \endcode
 *
 * The streams can be redirected at will by using the Logger accessor methods.
 *
 * \note The logging object returned by log() will not evalute the arguments if
 * no output will be displayed.
 */
/*!
 * \example src/core/test/tstLogger.cc
 *
 * Test of Logger.
 */
//===========================================================================//
class Logger
{
  public:
    //@{
    //! Public type aliases
    using ostream_t  = Logger_Statement::ostream_t;
    using SP_ostream = std::shared_ptr<ostream_t>;
    //@}

    //! \brief Default local log level
    static constexpr Log_Level default_local_level = Log_Level::INFO;

    //! \brief Default global log level
    static constexpr Log_Level default_global_level = Log_Level::INFO;

  private:
    // >>> IMPLEMENTATION DATA

    //! Local and global minimum log levels
    Log_Level d_local_level =  default_local_level;
    Log_Level d_global_level = default_global_level;

  private:
    Logger()
    : d_local_level(DIAGNOSTIC)
    , d_global_level(DIAGNOSTIC)
    , d_node(0)
    , d_screen_output("screen", DEBUG)
    , d_file_output("file",     END_LOG_LEVEL)
    {
        d_screen_output.stream_ptr.reset(&std::cerr, null_deleter());
    }

    Logger(const Logger&);
    Logger& operator=(const Logger&);

    // >>> STATIC DATA
    // Prefixes for debug/info/etc e.g. "***"
    static constexpr const char* const s_log_prefix[END_LOG_LEVEL] = {
        "      ", // DEBUG
        "      ", // DIAGNOSTIC
        ":::   ", // STATUS
        ">>>   ", // INFO
        "***   ", // WARNING
        "\n!!!   ", // ERROR
        "\n!*!*! ", // CRITICAL
    };

  public:
    // >>> CONFIGURATION

    // Set MINIMUM verbosity level for local log calls to be logged.
    void set_local_level(Log_Level level = default_local_level)
    {
        Require(0 <= level && level < END_LOG_LEVEL);
        d_local_level = level;
    }

    //! \brief Returns the local logging level
    Log_Level local_level() const noexcept
    { return d_local_level; }

    //! \brief Returns the global logging level
    Log_Level global_level() const noexcept
    { return d_global_level; }

    // Set MINIMUM verbosity level for global log calls to be logged.
    void set_global_level(Log_Level level = default_local_level)
    {
        Require(0 <= level && level < END_LOG_LEVEL);
        d_global_level = level;
    }

    // Set output stream from a shared pointer
    void set(const std::string &key,
             SP_ostream         stream_sp,
             Log_Level          min_level = default_local_level)
    {
       Require(stream_sp);
       Require(0 <= min_level && min_level < END_LOG_LEVEL);

       Sink& sink = find(key);

       sink.name = key;
       sink.level = min_level;
       sink.stream_ptr = stream_sp;
    }

    // Set output stream from a raw pointer (UNSAFE EXCEPT WITH GLOBAL OSTREAM!)
    void set(const std::string &key,
             ostream_t         *stream_ptr,
             Log_Level          min_level = default_local_level)
    {
       Require(stream_ptr);
       Require(0 <= min_level && min_level < END_LOG_LEVEL);

       Sink& sink = find(key);

       sink.name = key;
       sink.level = min_level;
       sink.stream_ptr = SP_ostream(stream_ptr, null_deleter());
    }

    // Remove an output stream
    void remove(const std::string &key)
    {
        Sink& sink = find(key);
        sink.stream_ptr.reset();
    }

    // >>> STREAMING

    // Return a stream appropriate to the level for node-zero output
    Logger_Statement global_stream(Log_Level level = default_local_level)
    {
        Require(0 <= level && level < END_LOG_LEVEL);

        Logger_Statement::Vec_Ostream streams;

        // Only add streams on node zero
        if (d_node == 0 && level >= d_global_level)
        {
            streams = build_streams(level);
        }

        // Create the logger statement (moving the vec streams for efficiency)
        Logger_Statement result(std::move(streams));

        // Pipe prefix to the stream before returning
        result << s_log_prefix[level];

        // Return the expiring logger_statement, implicit move
        return result;
    }

    // Return a stream appropriate to the level for local-node output
    Logger_Statement local_stream(Log_Level level = default_local_level)
    {
        Require(0 <= level && level < END_LOG_LEVEL);

        Logger_Statement::Vec_Ostream streams;

        if (level >= d_local_level)
        {
            streams = build_streams(level);
        }

        // Create the logger statement (moving the vec streams for efficiency)
        Logger_Statement result(std::move(streams));

        // Pipe prefix to the stream before returning
        result << s_log_prefix[level];

        // Return the expiring logger_statement, implicit move
        return result;
    }


  public:
    // >>> STATIC METHODS

    //! \brief Accesses the global logging instance
    static Logger& get()
    {
        static Logger s_instance;
        Check(std::end(s_log_prefix) - std::begin(s_log_prefix)
                == END_LOG_LEVEL);
        return s_instance;
    }


  private:
    //! Struct for output levels
    struct Sink
    {
        std::string name;  //!< Name of output sink
        Log_Level   level; //!< Output only if message >= this level
        SP_ostream  stream_ptr; //!< SP to keep pointer alive

        Sink(const std::string &n, Log_Level lev = default_local_level)
            : name(n)
            , level(lev)
            , stream_ptr()
        {
            /* * */
        }
    };

    using Vec_Ostream = Logger_Statement::Vec_Ostream;

    // Set the active stream
    void activate_stream(ostream_t* stream);

  private:
    //! Node ID, saved when Logger is first called.
    int d_node = 0;

    // Instead of doing something complicated like a sorted vector on name,
    // just have one sink for screen output, one for "log file" output
    Sink d_screen_output;
    Sink d_file_output;

    // Find the sink given this name
    Sink& find(const std::string &key)
    {
        if (key == "screen")
        {
            return d_screen_output;
        }
        else if (key == "file")
        {
            return d_file_output;
        }
        else
        {
            Validate(false, "Currently only screen and file are supported "
                     "log keys; '" << key << "' is invalid.");
        }

        // Squelch compiler errors
        return find(key);
    }

    // Build output streams based on the given level
    Vec_Ostream build_streams(Log_Level level = default_local_level) const
    {
        Require(0 <= level && level < END_LOG_LEVEL);
        Vec_Ostream streams;

        for (const Sink* s : {&d_screen_output, &d_file_output})
        {
            Check(s);
            if (s->stream_ptr && (level >= s->level))
            {
                streams.push_back(s->stream_ptr.get());
            }
        }
        return streams;
    }
};


//---------------------------------------------------------------------------//
//! Access the global logger instance
inline Logger& logger()
{
    return Logger::get();
}


//---------------------------------------------------------------------------//
//! Return an ostream for global (node zero only) messages
inline Logger_Statement log(Log_Level level = Logger::default_local_level)
{
    return Logger::get().global_stream(level);
}


//---------------------------------------------------------------------------//
//! Return an ostream for local messages
inline Logger_Statement log_local(
        Log_Level level = Logger::default_local_level)
{
    return Logger::get().local_stream(level);
}


//---------------------------------------------------------------------------//
//! Makes a new line of text that is flush with the message's level
inline const char* flush_next(
        Log_Level level = Logger::default_local_level)
{
    Require(0 <= level && level < END_LOG_LEVEL);
    return "\n      ";
}


//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_core_Logger_hh

//---------------------------------------------------------------------------//
// end of src/core/Logger.hh
//---------------------------------------------------------------------------//
