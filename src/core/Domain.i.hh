//---------------------------------------------------------------------------//
/*!
 * \file  src/core/Domain.i.hh
 * \brief Domain class function specifications.
 */
//---------------------------------------------------------------------------//
#ifndef src_core_Domain_i_hh
#define src_core_Domain_i_hh

#include <iostream>

namespace FET
{

//---------------------------------------------------------------------------//
/*!
 * \brief Describes the domain to a given output stream
 */
template<class T>
void Domain<T>::describe(std::ostream os) const noexcept
{
    os << "Domain: " << label() << "\n"
       << "   Lower limit = " << lower() << "\n"
       << "   Upper limit = " << upper() << "\n";
}


//---------------------------------------------------------------------------//
/*!
 * \brief Describes a Domain in-line
 */
template<class T>
void Domain<T>::describe_inline(std::ostream os) const noexcept
{
    os << label() << " domain: "
       << "[" << lower() << ", " << upper() << "]";
}


//---------------------------------------------------------------------------//
} // end namespace FET

#endif
//---------------------------------------------------------------------------//
// end of src/core/Domain.i.hh
//---------------------------------------------------------------------------//
