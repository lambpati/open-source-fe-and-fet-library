//---------------------------------------------------------------------------//
/*!
 * \file  src/Managers/test/Managed_Object.hh
 * \brief Managed_Object class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Managers_test_Managed_Object_hh
#define src_Managers_test_Managed_Object_hh

#include <string>
#include <vector>

#include "Name.hh"

#include "Particle.hh"

namespace FET
{

namespace TEST
{

//===========================================================================//
/*!
 * \class Managed_Object
 * \brief For testing purposes to demonstrate control flow of manager
 *
 * Simple object that emulates an FET object's methods. Acts as a placeholder
 * for an FET object while excluding the complexity of an FET.
 */
//===========================================================================//
template<class Scorer = double>
class Managed_Object
{
public:

    //@{
    //! Public type aliases
    using data_type       = double;
    using Vec_Dbl         = std::vector<double>;
    using Vec_Vec_Dbl     = std::vector<Vec_Dbl>;
    //@}

private:

    //! \brief Simple flag for uniqueness
    unsigned int b_id = 0;


public:

    //! \brief Simply constructor
    Managed_Object(const unsigned int id = 0)
        : b_id(id)
    { /* * */ }

    //! \brief Prints the ID of the object
    void print_ID(std::ostream& os = std::cout) const noexcept
    { os << "ID " << b_id << ": "; }

public:
    // >>> For base manager

    void describe(std::ostream& os) const noexcept
    {
        print_ID(os);
        os << "describe\n";
    }


public:
    // >>> Typical FET Object methods

    //! \brief Returns the label of the object
    std::string label() const noexcept
    { return std::to_string(b_id); }

    unsigned int id() const noexcept
    { return b_id; }

    //! \brief Resets the managed object
    void reset() noexcept
    {
        print_ID();
        std::cout << "reset\n";
    }

    //! \brief Alias for \c process
    void score(const Scorer& s)
    {
        print_ID();
        std::cout << "score with s = " << s << "\n";
    }

    //! \brief Finalizes the data contained in the Managed object
    void finalize(const data_type val = 1.0)
    {
        print_ID();
        std::cout << "finalize with val = " << val << "\n";
    }

    Vec_Vec_Dbl coefficients() const
    {
        Vec_Dbl v = {1,2,3};
        Vec_Vec_Dbl v2 = {v,v,v};
        return v2;
    }
    Vec_Vec_Dbl variance() const
    {
        Vec_Dbl v = {4,5,6};
        Vec_Vec_Dbl v2 = {v,v,v};
        return v2;
    }
    Vec_Dbl flux() const
    {
        Vec_Dbl v = {7,8,9};
        return v;
    }

}; // end class Managed_Object

 //---------------------------------------------------------------------------//
} // end namespace TEST
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_Managers_test_Managed_Object_hh
//---------------------------------------------------------------------------//
// end of src/Managers/test/Managed_Object.hh
//---------------------------------------------------------------------------//
