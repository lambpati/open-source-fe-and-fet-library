//---------------------------------------------------------------------------//
/*!
 * \file  src/Managers/test/tstManager_Base.cc
 * \brief Test of the Manager_Base object
 */
//---------------------------------------------------------------------------//

#include "Manager_Base.hh"

namespace FET
{
namespace TEST
{

  //! \brief Tests the \c Manager_Base object
  void TEST_Manager()
  {

  }

} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::TEST_Manager();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Managers/test/tstManager_Base.cc
//---------------------------------------------------------------------------//
