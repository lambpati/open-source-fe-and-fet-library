//---------------------------------------------------------------------------//
/*!
 * \file  src/Managers/Manager_Base.hh
 * \brief Manager_Base class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Managers_Manager_Base_hh
#define src_Managers_Manager_Base_hh

#include <map>
#include <string>
#include <vector>

#include "Name.hh"

#include "FET_Base.hh"
#include "Particle.hh"

namespace FET
{

//===========================================================================//
/*!
 * \def MethodLoop(method)
 * \brief Provides quick method to loop over contained objects with args
 * \param method Method name and all associaged arguments
 *
 * MethodLoops over the manager container to execute the given method for each
 * container element. Method should include all arguments necessary for
 * execution.
 */
#define MethodLoop(METHOD) \
    for (auto& it : d_data)\
    { it.METHOD; }


//===========================================================================//
/*!
 * \def DerivedMethodLoop(method)
 * \brief Same as MethodLoop, but for derived objects
 * \param method Method name and all associaged arguments
 *
 * Used by derived classes of Manager_Base to loop over all contained elements
 * easily to execute a common method.
 *
 * \see MethodLoop
 */
#define DerivedMethodLoop(METHOD) \
    for (auto& it : Base::d_data)\
    { it.METHOD; }


//===========================================================================//
/*!
 * \class Manager_Base
 * \brief Basic FET manager object
 *
 * Contains any number of objects desired by the consumer. Extends underlying
 * container functionality to the consumer to appear ``as-if'' the consumer is
 * strictly using a C++ container object.
 *
 * Managers may also be managers of similar managers. That is, a manager of
 * objects with 'pre_process', 'process', and 'post_process' methods will
 * provide a same named method. Therefore, a manager containing those managers
 * will provide the same functionality to its sub-objects, thus being flexible
 * for such managers.
 *
 * Classes the derive from this can access the data via `Base::d_data`, with
 * `using Base = Manager_Base<T>;`
 *
 * As an example, we can create a manager of object "Foo<double, double>"
 * named "Bar<Foo>", with specialized methods relating to the "Foo" object.
 * A manager of "Bar" objects will further have a similar structure,
 * named "Soo", being of type "Bar<Bar>". For an example, see \ref
 * tstSuperManager.cc
 *
 * \note This object should only be used on non-pointer-like objects.
 */
/*!
 * \example src/Managers/test/tstBaseManager.cc
 *
 * Test of the Manager_Base object
 */
/*!
 * \example src/Managers/test/tstSuperManager.cc
 *
 * Example of a manage of managers
 */
//===========================================================================//
template<class managed_FET>
class Manager_Base : public Name
{
    using Base     = Name;
public:

    //@{
    //! Public type aliases
    using container       = std::vector<managed_FET>;
    //using key_type        = typename container::key_type;
    using iterator        = typename container::iterator;
    using const_iterator  = typename container::const_iterator;
    using size_type       = typename container::size_type;
    //using data_type       = typename managed_FET::data_type;
    using Vec_Dbl         = std::vector<double>;
    using Vec_Vec_Dbl     = std::vector<Vec_Dbl>;
    //@}
    container d_data;

public:
    // >>> CONSTRUCTOR/DESTRUCTORS

    //! \brief Basic Constructor
    Manager_Base(const Base::string& name,
                 const Base::string& desc = "",
                 const std::vector<managed_FET>& items = {})
        : Base(name, desc)
    { insert(items); }

    //@{
    //! Copy control (copy, move, and destructor)
    Manager_Base(const Manager_Base&) = default;
    Manager_Base& operator=(const Manager_Base&) = default;
    Manager_Base(Manager_Base&&) = default;
    Manager_Base& operator=(Manager_Base&&) = default;
    virtual ~Manager_Base() = default;
    //@}


public:
    // >>> CONTAINER INTERFACE

    // >>> Capacity
    //! \brief Test whether container is empty
    bool empty() const noexcept
    { return d_data.empty(); }

    //! \brief Return container size
    size_type size() const noexcept
    { return d_data.size(); }

    // >>> Element access
    //! \brief Access element
    //managed_FET& operator[](const int key)
    //{ return d_data[key]; }
    //managed_FET& operator[](const int key)
    //{ return d_data[key]; }

    //! \brief Access element with bounds checking
    managed_FET& at(const int key)
    { return d_data[key]; }
    const managed_FET& at(const int key) const
    { return d_data[key]; }

    // >>> Modifiers
    //! \brief Insert element
    void insert(const managed_FET& object)
    { d_data.push_back(object); }

    //! \brief Insert a vector of elements
    template<class InputIterator>
    void insert(const std::vector<managed_FET> &items)
    {
        for(unsigned int i=0;i<items.size();++i)
            d_data.push_back(items[i]);
    }

    //! \brief Erase element by key
    void erase(const int key)
    {
        d_data.erase(d_data.find(key));
    }

    //! \brief Erase a range of elements by iterator
    iterator erase(
            const_iterator first,
            const_iterator last)
    { d_data.erase(first, last); }

    //! \brief Swaps the contents of one container and another
    template<class Manager>
    void swap(Manager& item)
    { d_data.swap(item); }

    //! \brief Clears the contents of the container
    void clear() noexcept
    { d_data.clear(); }

    // >>> Operations
    //! \brief Finds and returns the location of the object with matching key if it exists
    int find(const int key)
    {
        for(unsigned int i=0;i<d_data.size();++i)
        {
            if(d_data[i].id()==key)
                return key;
        }
        return -1;
    }

    //! \brief Counts the number of elements matching a key
    size_type count(const int key) const
    {
        //return d_data.count(key);
    }


public:
    // >>> SETTERS

    //! \brief Inserts a list of elements
    void insert(const std::vector<managed_FET>& list)
    {
        for (auto& it : list)
        { insert(it); }
    }


public:
    // >>> GETTTERS

    virtual Vec_Vec_Dbl coefficients() const
    {
        Not_Implemented("get_coeff()");
    }
    virtual Vec_Vec_Dbl variance() const
    {
        Not_Implemented("get_variance()");
    }
    virtual Vec_Dbl flux() const
    {
        Not_Implemented("get_flux()");
    }

    virtual Vec_Vec_Dbl locations() const
    {
        Not_Implemented("get_locations()");

    }

    virtual Vec_Dbl energies() const
    {
        Not_Implemented("get_energies()");
    }

    //! \brief Returns the FET object type
    virtual const std::string type() const noexcept
    { return "Base Manager"; }

    //! \brief Flags if the key exists in the container
    bool contains(const int key) const
    { return d_data.find(key) > -1; }

public:
    // >>> INTROSPECTION

    //! \brief Describes all managed objects
    void describe(std::ostream& os) const { MethodLoop(describe(os)); }


}; // end class Manager_Base

 //---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_Managers_Manager_Base_hh
//---------------------------------------------------------------------------//
// end of src/Managers/Manager_Base.hh
//---------------------------------------------------------------------------//
