//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/Cosine_Attr.hh
 * \brief  Cosine_Attr class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Attr_Cosine_Attr_hh
#define src_Polynomials_Attr_Cosine_Attr_hh

#include "Constants.hh"
#include "Basis_Attr.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Cosine_Attr
 * \brief Basis set attributes for Cosine polynomials
 *
 * The Cosine_Attr class contains the specific details of a Cosine
 * polynomial basis set.
 */
/*!
 * \example src/Polynomials/Attr/test/tstCosine_Attr.cc
 *
 * Test of Cosine_Attr
 */
//===========================================================================//
class Cosine_Attr : public Basis_Attr
{
    using Base              = Basis_Attr;

  private:
    // >>> IMPLEMENTATION DATA

    //! \brief The basis set's orthogonal domain
    const Base::domain_type b_ortho_domain =
        Base::domain_type("Normalized", -pi, pi);


  public:
    // >>> CONSTRUCTION/SETTERS

    //! \brief Constructor for common arguments
    Cosine_Attr(
            const std::string& name,
            const Base::order_type order,
            const Base::domain_type& dom)
        : Base(name, order, dom)
   { /* * */ }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Cosine_Attr() = default;
    Cosine_Attr(const Cosine_Attr&) = default;
    Cosine_Attr& operator=(const Cosine_Attr&) = default;
    Cosine_Attr(Cosine_Attr&&) = default;
    Cosine_Attr& operator=(Cosine_Attr&&) = default;
    virtual ~Cosine_Attr() = default;
    //@}


  public:
    // >>> INTROSPECTION/GETTERS


    //! \brief Return the type description
    const std::string type() const noexcept override
    { return "Cosine"; }

    //! \brief Returns the standardized domain of the basis set
    const Base::domain_type& ortho_domain() const noexcept override
    { return b_ortho_domain; }

}; // end class Cosine_Attr

//---------------------------------------------------------------------------//
} // end namespace FET

#endif // src_Polynomials_Attr_Cosine_Attr_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/Cosine_Attr.hh
//---------------------------------------------------------------------------//
