//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/Legendre_Attr.hh
 * \brief  Legendre_Attr class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Attr_Legendre_Attr_hh
#define src_Polynomials_Attr_Legendre_Attr_hh

#include "Basis_Attr.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Legendre_Attr
 * \brief Basis set attributes for Legendre polynomials
 *
 * The Legendre_Attr class contains the specific details of a Legendre
 * polynomial basis set.
 */
/*!
 * \example src/Polynomials/Attr/test/tstLegendre_Attr.cc
 *
 * Test of Legendre_Attr
 */
//===========================================================================//
class Legendre_Attr : public Basis_Attr
{
    using Base              = Basis_Attr;

  private:
    // >>> IMPLEMENTATION DATA

    //! \brief The basis set's orthogonal domain
    const Base::domain_type b_ortho_domain =
        Base::domain_type("Normalized", -1.0, 1.0);

  public:
    // >>> CONSTRUCTION/SETTERS

    //! \brief Constructor for common arguments
    Legendre_Attr(
            const std::string& name,
            const Base::order_type order,
            const Base::domain_type& dom)
        : Base(name, order, dom)
    { /* * */ }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Legendre_Attr() = default;
    Legendre_Attr(const Legendre_Attr&) = default;
    Legendre_Attr& operator=(const Legendre_Attr&) = default;
    Legendre_Attr(Legendre_Attr&&) = default;
    Legendre_Attr& operator=(Legendre_Attr&&) = default;
    virtual ~Legendre_Attr() = default;
    //@}


  public:
    // >>> INTROSPECTION/GETTERS

    //! \brief Return the type description
    const std::string type() const noexcept override
    { return "Legendre"; }

    //! \brief Returns the standardized domain of the basis set
    const Base::domain_type& ortho_domain() const noexcept override
    { return b_ortho_domain; }


}; // end class Legendre_Attr

//---------------------------------------------------------------------------//
} // end namespace FET

#endif // src_Polynomials_Attr_Legendre_Attr_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/Legendre_Attr.hh
//---------------------------------------------------------------------------//
