//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/test/tstSine_Attr.cc
 * \brief  Sine_Attr class test.
 */
//---------------------------------------------------------------------------//

#include "FET_DBC.hh"

#include "Constants.hh"
#include "Sine_Attr.hh"

namespace FET
{
namespace TEST
{

  //! Tests for Sine class and derived class functionality
  void TEST_Sine_Attr()
  {
      // Create a Sine_Attr object
      FET::Sine_Attr attr;

      // Test attributes that differ from the Basis_Attr
      Insist(attr.type() == "Sine",
              "The wrong type was specified for Sine attributes.");
      Insist(attr.ortho_domain().lower() == -pi &&
              attr.ortho_domain().upper() == pi &&
              attr.ortho_domain().range() == 2.0 * pi,
              "The Sine series' normalized domain was not properly "
                 << "specified.");
  }

} // End of namespace TEST
} // End of namespace FET


int main()
{
  FET::TEST::TEST_Sine_Attr();
  return 0;
}


//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/test/tstSine_Attr.cc
//---------------------------------------------------------------------------//
