//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/test/tstChebyshev_Attr.cc
 * \brief  Chebyshev_Attr class test.
 */
//---------------------------------------------------------------------------//

#include "FET_DBC.hh"
#include "Chebyshev_Attr.hh"

namespace FET
{
namespace TEST
{

  //! Tests for Chebyshev class and derived class functionality
  void TEST_Chebyshev_Attr()
  {
      // Create a Chebyshev_Attr object
      FET::Chebyshev_Attr attr;

      // Test attributes that differ from the Basis_Attr
      Insist(attr.type() == "Chebyshev",
              "The wrong type was specified for Cosine attributes.");
      Insist(attr.ortho_domain().lower() == -1.0 &&
              attr.ortho_domain().upper() == 1.0 &&
              attr.ortho_domain().range() == 2.0,
              "The Chebyshev series' normalized domain was not properly "
                 << "specified.");
  }

} // End of namespace TEST
} // End of namespace FET


int main()
{
  FET::TEST::TEST_Chebyshev_Attr();
  return 0;
}


//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/test/tstChebyshev_Attr.cc
//---------------------------------------------------------------------------//
