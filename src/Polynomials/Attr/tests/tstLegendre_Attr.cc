//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/test/tstLegendre_Attr.cc
 * \brief  Legendre_Attr class test.
 */
//---------------------------------------------------------------------------//

#include "FET_DBC.hh"
#include "Legendre_Attr.hh"

namespace FET
{
namespace TEST
{

  //! Tests for Legendre class and derived class functionality
  void TEST_Legendre_Attr()
  {
      // Create a Legendre_Attr object
      FET::Legendre_Attr attr;

      // Test attributes that differ from the Basis_Attr
      Insist(attr.type() == "Legendre",
              "The wrong type was specified for Legendre attributes.");
      Insist(attr.ortho_domain().lower() == -1.0 &&
              attr.ortho_domain().upper() == 1.0 &&
              attr.ortho_domain().range() == 2.0,
              "The Legendre series' normalized domain was not properly "
                 << "specified.");
  }

} // End of namespace TEST
} // End of namespace FET


int main()
{
  FET::TEST::TEST_Legendre_Attr();
  return 0;
}


//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/test/tstLegendre_Attr.cc
//---------------------------------------------------------------------------//
