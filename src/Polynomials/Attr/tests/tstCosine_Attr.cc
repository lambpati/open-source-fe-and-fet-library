//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/test/tstCosine_Attr.cc
 * \brief  Cosine_Attr class test.
 */
//---------------------------------------------------------------------------//

#include "FET_DBC.hh"

#include "Constants.hh"
#include "Cosine_Attr.hh"

namespace FET
{
namespace TEST
{

  //! Tests for Cosine class and derived class functionality
  void TEST_Cosine_Attr()
  {
      // Create a Cosine_Attr object
      FET::Cosine_Attr attr;

      // Test attributes that differ from the Basis_Attr
      Insist(attr.type() == "Cosine",
              "The wrong type was specified for Cosine attributes.");
      Insist(attr.ortho_domain().lower() == -pi &&
              attr.ortho_domain().upper() == pi &&
              attr.ortho_domain().range() == 2.0 * pi,
              "The Cosine series' normalized domain was not properly "
                 << "specified.");
  }

} // End of namespace TEST
} // End of namespace FET


int main()
{
  FET::TEST::TEST_Cosine_Attr();
  return 0;
}


//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/test/tstCosine_Attr.cc
//---------------------------------------------------------------------------//
