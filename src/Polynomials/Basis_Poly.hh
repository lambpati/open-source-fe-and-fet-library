//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Basis_Poly.hh
 * \brief  Basis_Poly class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Basis_Poly_hh
#define src_Polynomials_Basis_Poly_hh

#include <iostream>
#include <memory>
#include <string>
#include <type_traits>
#include <vector>

#include "FET_DBC.hh"
#include "Basis_Attr.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Basis_Poly
 * \brief Structure of polynomial basis sets for utilization in FETs
 *
 * Structure of polynomial basis sets for utilization in FETs. Such basis sets
 * include Legendre, Fourier, Hermite, Laguerre, Zernike, etc. Other methods
 * ought to only need to overload the `evaluated_polynomial` and
 * `get_order_normalization` functions.
 *
 * Clients may use classes of this base primarily with the constructor and the
 * `evaluate(domain_value)` function. Usage of normalization, filter
 * application, etc., are all set by the basis set's attributes class (based on
 * the `Basis_Poly` class)
 */
/*!
 * \example src/Polynomials/test/tstBasis_Poly.cc
 *
 * Test of \c Basis_Poly
 */
//===========================================================================//
template<typename T = double>
class Basis_Poly
{
  public:
    //@{
    //! Public type aliases
    using data_type   = T;
    using Attr_type   = FET::Basis_Attr;
    using order_type  = Attr_type::order_type;
    using SP_Attr     = std::shared_ptr<const Attr_type>;
    using set_type    = std::vector<T>;
    //@}

  private:
    //! >>> IMPLEMENTATION DATA

    //! \brief Basis set attributes
    SP_Attr d_attr = nullptr;


  public:
    // >>> CONSTRUCTOR/DESTRUCTORS

    //! \brief Simple constructor
    template<class U>
    Basis_Poly(const U& attr)
    {
        Insist(std::is_floating_point<data_type>::value,
                "The polynomial must utilize a valid floating-point type.");
        set_attribute(attr);
    }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Basis_Poly() = default;
    Basis_Poly(const Basis_Poly&) = default;
    Basis_Poly& operator=(const Basis_Poly&) = default;
    Basis_Poly(Basis_Poly&&) = default;
    Basis_Poly& operator=(Basis_Poly&&) = default;
    virtual ~Basis_Poly() = default;
    //@}


  public:
    // >>> GETTERS

    //! \brief Returns the basis attribute
    const SP_Attr attr() const noexcept
    { return d_attr; }

    //! \brief Return the basis set type description
    const std::string type() const noexcept
    { return attr()->type(); }

    //! \brief Returns the order of the basis set
    order_type order() const noexcept
    { return attr()->order(); }

    //! \brief Alias for \c order
    order_type degree() const noexcept
    { return order(); }

    //! \brief Returns the number of terms being used for the basis set
    order_type num_terms() const noexcept
    { return order() + 1; }

    /*!
     * \brief Returns if the location is contained by the polynomial
     *
     * Flags if the value provided is contained within the polynomial's
     * simulation domain.
     */
    bool contains(const data_type x) const noexcept
    { return attr()->ortho_domain().contains(x); }


  public:
    // >>> SETTERS

    //! \brief Sets the attributes for the object
    template<class U>
    void set_attribute(const U& attr) noexcept
    { d_attr = std::make_shared<const U>(attr); }


  public:
    // >>> INTROSPECTION

    //! \brief Write an ReST-formatted descriptoin of the basis set
    void describe(std::ostream& os) const noexcept
    { attr()->describe(os); }

    /*!
     * \brief Returns the volume occupied by the domain
     *
     * Returns the volume occupied by the polynomial or basis set.
     *
     * Default implementation is for 1-D polynomials, simply being the span of
     * the domain. For multi-dimensional polynomials, a different method
     * for the volume is required.
     * \todo Change to \c data_type AFTER \c attr uses \c Domain<T>
     */
    virtual double volume() const noexcept
    { return attr()->domain().range(); }

    // Standardizes a given value to the orthonormalized range
    virtual data_type standardize(data_type domain_value) const noexcept;

    // Evaluates the polynomial series at a given value and order
    virtual set_type evaluate(const order_type order, const data_type x)
        const;

    //! \brief Evaluates the polynomial series at its specified order
    virtual set_type evaluate(const data_type x) const
    { return evaluate(degree(), x); }

    //! \brief Alias for \c evaluate(x)
    set_type operator()(const data_type x) const
    { return evaluate(x); }

    //! \brief Alias for \c evaluate(order, x)
    set_type operator()(const order_type order, const data_type x) const
    { return evaluate(order, x); }

    // Normalizes the polynomials on the simulation domain
    virtual void normalize(set_type& poly_val) const noexcept;

    // Normalizes the polynomials on the orthogonal domain
    virtual void ortho_normalize(set_type& poly_val) const noexcept;

    virtual double orthonorm_const(int order) const noexcept;

    // Returns the value of the weighting function for the basis set
    virtual data_type weight_fct(const data_type value) const noexcept;

    // Integrates the basis set on a given interval
    virtual set_type integrate(const data_type lower_b,
            const data_type upper_b,
            const order_type order)
        const;

    //! \brief Integrates the basis set on the interval through specified order
    virtual set_type integrate(const data_type lower_b,
            const data_type upper_b) const
    { return integrate(lower_b, upper_b, order()); }

    // Checks for equality of basis sets
    bool operator==(const Basis_Poly<T>& poly) const noexcept;


}; // end class Basis_Poly

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// INLINE AND TEMPLATE DEFINITIONS
//---------------------------------------------------------------------------//
#include "Basis_Poly.i.hh"

//---------------------------------------------------------------------------//
#endif // src_Polynomials_Basis_Poly_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Basis_Poly.hh
//---------------------------------------------------------------------------//
