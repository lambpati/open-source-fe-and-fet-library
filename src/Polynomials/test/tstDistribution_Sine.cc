//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/test/tstDistribution_Sine.cc
 * \brief  Tests the Sine series distribution
 */
//---------------------------------------------------------------------------//

#include "Domain.hh"
#include "Sine_Attr.hh"
#include "Sine_Poly.hh"

#include "make_distribution.cc"

namespace FET
{
namespace TEST
{

  //! \brief Tests the Sine series distribution
  void Sine_Distribution()
  {
    // Create some domain for the basis set
    Domain<double> domain("x", -20.0, 20.0);

    // Create the attributes for the basis set
    Sine_Attr attrs("A single Sine polynomial", 15, domain);
    attrs.set_normalize(false);

    // Create the Sine basis set now
    Sine_Poly<> poly(attrs);

    // Now print the distribution
    make_distribution(poly);

    return;
  }

} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::Sine_Distribution();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Polynomials/test/tstSine_Orders.cc
//---------------------------------------------------------------------------//
