"""
Plots data within a data file containing the evaluation of Polynomials, such as
Legednre, Cosine, Hermite, etc., over the entire range in the data file.
"""
import argparse
import matplotlib.pyplot as plt
import os
import pandas as pd
import signal
import sys

from matplotlib import rc
# rc('text', usetex=True)

class Polynomial_Plotter:
    """
    Reads data from a file to generate plots of read polynomial data.
    """

    def __init__(self):
        """Constructor"""
        # Allow some keyboard interupts
        signal.signal(signal.SIGINT, signal.SIG_DFL)

        # Create arguments from command line
        self.__setup_parser()
        self.__parse()

        return

    def __setup_parser(self):
        """Creates arguments for the argument parser"""
        # Create object
        self.__parser = argparse.ArgumentParser(
            description="Plots polynomials of some type to a specified order.")

        # Add required positionals
        self.__parser.add_argument("type", type=str,
            choices=["Chebyshev", "Cosine", "Hermite", "Legendre", "Sine"],
            help="Polynomial type (e.g. Legendre)")
        self.__parser.add_argument("order",
            help="Polynomial order (e.g. 12); < 0 uses default", type=int)

        # Add optionals
        self.__parser.add_argument("-f", "--file", default="",
            type=str, help="File name containing the data")
        self.__parser.add_argument("-d", "--dir",
            default="./Data/", type=str,
            help="Data file directory")
        self.__parser.add_argument("-p", "--prefix",
            default="n = ", type=str, help="Legend label prefix")
        self.__parser.add_argument("-a", "--append",
            default="", type=str, help="Text to append to the legend label")
        self.__parser.add_argument("-x", "--xlabel",
            default="x [units]", type=str, help="X-axis label")
        self.__parser.add_argument("-y", "--ylabel",
            default="", type=str, help="Y-axis label")
        self.__parser.add_argument("-s", "--save",
            default=False, type=bool,
            help="Saves (=True) or shows (=False) the generated plot")
        self.__parser.add_argument("--dpi", default=1200, type=int,
            choices=range(300, 1200+1, 300),
            help="DPI at which to save the file")

        return

    def __parse(self):
        """Parses the arguments"""
        self.__args = self.__parser.parse_args()
        return

    def __read_data(self):
        """Reads data into a Pandas dataframe from a CSV file"""


        # Set default order values
        if (self.__args.order < 0):
            if (self.__args.type == "Chebyshev"):
                self.__args.order = 5
            elif (self.__args.type == "Cosine"):
                self.__args.order = 3
            elif (self.__args.type == "Hermite"):
                self.__args.order = 4
            elif (self.__args.type == "Legendre"):
                self.__args.order = 9
            elif (self.__args.type == "Sine"):
                self.__args.order = 3
            else:
                self.__args.order = 12

        # Postprocess path/to/file
        # (set default is applicable)
        if (self.__args.file == ""):
            self.__args.file = "{}_Distribution.txt".format(self.__args.type)
        file_name = "{}".format(self.__args.file)
        # (if it doesn't exist, check if exists in specified path)
        if (not os.path.exists(file_name)):
            file_name = "{}{}".format(self.__args.dir, file_name)
        # (file doesn't exist regardless of name or path/to/name)
        if (not os.path.exists(file_name)):
            sys.exit("The data file '{}' does not exist.".format(file_name))

        # Open the file and do some prelim
        skip_lines = list(range(0, 19))
        skip_lines.append(20)

        # Read the data
        self.__data = pd.read_csv(file_name, sep = r",\s*",
            skiprows = skip_lines, engine="python", header=0)

        # Ensure data for the requested order exists
        if (self.__args.order > len(self.__data.columns) - 3) or (self.__args.order < 0):
            sys.exit("Order {} likely does not exist in the dataframe.".format(
                self.__args.order))

        return

    def __plot(self):
        """Plot the distribution of polynomials"""
        # Plot the data:
        domain = "x_std" # or "x_sim"
        for order in range(0, self.__args.order + 1):
            legend_label = "{}{}{}".format(self.__args.prefix, order, self.__args.append)
            order_label = "n = {}".format(order)
            plt.plot(self.__data[domain], self.__data[order_label],
                    label = legend_label,
                    linewidth = 1)

        # Apply figure labels
        plt.xlabel(self.__args.xlabel)
        if (self.__args.ylabel == ""):
            self.__args.ylabel = "{} Polynomial Value".format(self.__args.type)
        plt.ylabel(self.__args.ylabel)
        plt.title("{} Polynomial Distribution up to Order {}".format(self.__args.type,
            self.__args.order))
        plt.legend(loc="best")
        plt.grid()
        # plt.locator_params(axis='x', nbins=5)
        # plt.locator_params(axis='y', nbins=5)
        if (self.__args.save):
            plt.savefig("{}_Distribution.png".format(self.__args.type),
                    dpi=self.__args.dpi)
        else:
            plt.show()

        return

    def generate(self):
        """Reads the data and generates the plot"""
        self.__read_data()
        self.__plot()
        return

if __name__ == "__main__":
    """Main control flow"""
    plotter = Polynomial_Plotter()
    plotter.generate()
