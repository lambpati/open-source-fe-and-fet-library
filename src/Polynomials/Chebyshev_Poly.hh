//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Chebyshev_Poly.hh
 * \brief  Chebyshev_Poly class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Chebyshev_Poly_hh
#define src_Polynomials_Chebyshev_Poly_hh

#include <math.h>
#include <memory>
#include <vector>

#include "FET_Macros.hh"
#include "FET_DBC.hh"
#include "Basis_Poly.hh"
#include "Chebyshev_Attr.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Chebyshev_Poly
 * \brief Chebyshev Polynomial implementation
 *
 * Class for the implementation of Chebyshev polynomials. Based on the
 * Basis_Poly class and utilizes the Chebyshev_Attr class for its associated
 * attributes.
 */
/*!
 * \example src/Polynomials/test/tstChebyshev_Poly.cc
 *
 * Test of the Chebyshev polynomial.
 */
//===========================================================================//
template<typename T = double>
class Chebyshev_Poly : public Basis_Poly<T>
{

  public:
    //@{
    //! Public type aliases
    using Base        = Basis_Poly<T>;
    using order_type  = typename Base::order_type;
    using data_type   = typename Base::data_type;
    using set_type    = typename Base::set_type;
    //@}


  public:
    // >>> CONSTRUCTOR/DESTRUCTORS

    //! \brief Constructor
    Chebyshev_Poly(const Chebyshev_Attr& attr)
        : Base(attr)
    {
        Insist(attr.type().compare("Chebyshev") == 0,
                "Chebyshev basis sets may only accept Chebyshev attributes ("
                << attr.type() << " received)");
    }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Chebyshev_Poly() = default;
    Chebyshev_Poly(const Chebyshev_Poly&) = default;
    Chebyshev_Poly& operator=(const Chebyshev_Poly&) = default;
    Chebyshev_Poly(Chebyshev_Poly&&) = default;
    Chebyshev_Poly& operator=(Chebyshev_Poly&&) = default;
    virtual ~Chebyshev_Poly() = default;
    //@}


  public:
    // >>> GETTERS
    // N/A presently


  public:
    // >>> SETTERS
    // N/A presently


  public:
    // >>> INTROSPECTION

    // Evaluates the polynomials for the basis set at a domain value
    set_type evaluate(const order_type order, const data_type domain_val)
        const override;

    // Normalizes a set of data on the simulation domain
    void normalize(set_type& poly_values) const noexcept override;

    // Normalizes a set of data on the orthogonal domain
    void ortho_normalize(set_type&) const noexcept override;

    // Returns the weighting function for Chebyshev polynomials
    data_type weight_fct(const data_type val) const noexcept override
    {
        return 1.0 / sqrt(1 - pow(
                    (std::abs(val) > 0.99 ? 0.99 : val),
                    2.0));
    }

    // Returns a vector of integrated values on a given std. domain
    set_type integrate(const data_type lower_b, const data_type upper_b,
            const order_type order)
        const override;

    //! \brief Uses default order to evaluate the polynomial
    set_type evaluate(const data_type domain_val) const override
    { return evaluate(Base::order(), domain_val); }

    //! \brief Uses the default order to evaluate the integration
    set_type integrate(const data_type lower_b, const data_type upper_b)
        const override
    { return integrate(lower_b, upper_b, Base::order()); }

}; // end class Chebyshev_Poly

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// INLINE AND TEMPLATE DEFINITIONS
//---------------------------------------------------------------------------//
#include "Chebyshev_Poly.i.hh"

//---------------------------------------------------------------------------//
#endif // src_Polynomials_Chebyshev_Poly_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Chebyshev_Poly.hh
//---------------------------------------------------------------------------//
