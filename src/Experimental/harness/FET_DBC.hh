//---------------------------------------------------------------------------//
/*!
 * \file  src/harness/FET_DBC.hh
 * \brief FET_DBC method declaration.
 * \note  Copyright (c) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
 */
//---------------------------------------------------------------------------//
#ifndef src_harness_FET_DBC_hh
#define src_harness_FET_DBC_hh

#include <sstream>
#include <string>

#include "FET_Macros.hh"
#include "FET_Assertions.hh"

namespace FET
{

//============================================================================//
/*!
 * \page FET Design-by-Contract macros
 *
 * \section ddbc Using the FET Design-by-Contract macros
 *
 * \note Creation of the Design-by-Contract macros here are based HEAVILY on
 * the FET_DBC macros used within the FET environment hosted by the Shift team
 * at ORNL. The FET_DBC implementation stems from that of Tom Evans work at ORNL.
 *
 * The Design-by-Contract (FET_DBC) macros are intended to be used for validation
 * preconditions, interim results, and postconditions within methods. These
 * conditions must be true in order for the code following the FET_DBC macro to be
 * correct. For example,
 *
 * \code
 * Check( x > 0.0 );
 * y - sqrt( x );
 * \endcode
 *
 * If the assertion provided by \c Check fails, the code should throw an error.
 * These macros should be used to ferret out bugs in preceeding code, ensuring
 * that results are within reasonable bounds before proceeding to use the
 * created results in further calculations, etc.
 *
 * These macros are provided to support the FET_DBC formalism. The activiation of
 * each macro is keyed off a bit in the FET_DBC macro which can be specified on the
 * command line:
\verbatim
   Bit    FET_DBC macro affected
   ---    ------------------
    0     Require
    1     Check
    2     Ensure
\endverbatim
 *
 * So for instance, \c -DFET_DBC_Level=7 turns them all on, \c -DFET_DBC_Level=0 turns them
 * all off, and \c -DFET_DBC_Level=1 turns on \c Require but turns off \c Check and
 * \c Ensure. The default is to have them all enabled.
 *
 * The \c Insist macro is used for checking for unusual but fatal conditions in
 * the program runtime; whether or not the issue is due to developer or user
 * error, the condition must be met.
 *
 * Finally, the \c Validate macro is for explicitly checking user input for
 * consistency: it may check whether values are positive, etc. It should not be
 * used to test for internal conditions.
 *
 * Both \c Insist and \c Validate support streaming operators; use them to
 * clarify the error messages by specifying what the erroneous user-given value was.
 *
 * \note These macros provide a means to eliminate assertions, but not
 * insistings. The idea is that \c Check performs pre- and post-conditions
 * checks during program development which are desired to be removed for
 * production runs for performance sake. \c Insist is used for things that must
 * be true at all times, such as a file's existence.
 */
//============================================================================//

//============================================================================//
/*!
 * \def FET_DBC_Level
 * \brief Flags to print file and line of an assertion when thrown
 *
 * The \c FET_DBC_Level macro specifies what FET_DBC checks/validations are used during
 * compilation of the project.
 *
 * This is OFF for Release builds (=0), and ON for DEBUG builds (=7).
 *
 * The \c FET_DBC_Level defaults to 7 for Debug builds.
 *
 * \note The FET library uses CMake to define this variable. The definition
 * should \em never need to declared here.
 */
//============================================================================//
#if !defined(FET_DBC_Level)
#define FET_DBC_Level 7
#endif


//============================================================================//
/*!
 * \def FET_ASSERT_(condition)
 *
 * Throws an \c FET::assertion when a condition is not met
 */
//============================================================================//
#define FET_ASSERT_(COND) \
    do { if (FET_UNLIKELY(!(COND))) ::FET::toss_cookies(\
            #COND, __FILE__, __LINE__); } while(0)


//============================================================================//
/*!
 * \def FET_NOASSERT_(condition)
 *
 * Does not throw an \c FET::assertion from whether or not the condition fails
 */
//============================================================================//
#define FET_NOASSERT_(COND) \
    do { if (false && (COND)) {} } while (0)


//============================================================================//
/*!
 * \def Require(condition)
 *
 * Pre-condition checking macro. On when FET_DBC_Level & 1 is true.
 */
//============================================================================//
#ifndef Require
#if FET_DBC_Level & 1
#define REQUIRE_ON
#define Require(c) FET_ASSERT_(c)
#else
#define Require(c) FET_NOASSERT_(c)
#endif
#endif


//============================================================================//
/*!
 * \def Check(condition)
 *
 * Intra-scope checking macro. On when FET_DBC_Level & 2 is true.
 */
//============================================================================//
#ifndef Check
#if FET_DBC_Level & 2
#define Check(c) FET_ASSERT_(c)
#else
#define Check(c) FET_NOASSERT_(c)
#endif
#endif


//============================================================================//
/*!
 * \def Ensure(condition)
 *
 * Post-condition checking macro. On when FET_DBC_Level & 4 is true.
 */
//============================================================================//
#ifndef Ensure
#if FET_DBC_Level & 4
#define ENSURE_ON
#define Ensure(c) FET_ASSERT_(c)
#else
#define Ensure(c) FET_NOASSERT_(c)
#endif
#endif


//============================================================================//
/*!
 * \def Remeber(code)
 *
 * Add code to compilable code. On when FET_DBC_Level & 4 is true. Used in the
 * following manner:
 * \code
 *    Remember (int old = x;)
 *    // ...
 *    Ensure (x == old);
 *  \endcode
 */
//============================================================================//
#ifndef Remember
#if FET_DBC_Level & 4
#define REMEMBER_ON
#define Remember(c) c
#else
#define Remeber(c)
#endif
#endif


//============================================================================//
/*!
 * \def Insist(condition, message)
 *
 * Inviolate check macro. \c Insist is ALWAYS on.
 */
//============================================================================//
#ifndef Insist
#define Insist(COND, MSG_STREAM) \
    do \
    { \
        if (FET_UNLIKELY(!(COND))) \
        { \
            std::ostringstream msg; \
            msg << MSG_STREAM; \
            ::FET::toss_insist_cookies(#COND, msg.str(), \
                    __FILE__, __LINE__); \
        } \
    } while (0)
#endif


//============================================================================//
/*!
 * \def Not_Implemented(feature)
 *
 * Throw an error when the given feature string is not implemented. Always on.
 *
 * If FET_DBC_Level is non-zero, then the file and line of failure is printed.
 * Otherwise, this information is hidden from the user.
 */
//============================================================================//
#ifndef Not_Implemented
#define Not_Implemented(MSG) \
    throw ::FET::not_implemented_error(MSG, __FILE__, __LINE__)
#endif


//============================================================================//
/*!
 * \def Not_Configured(feature)
 *
 * Throw an error when the given feature is not enabled in the current build
 * configuration.
 *
 * If FET_DBC_Level is non-zero, then the file and line of failure is printed.
 * Otherwise, this information is hidden from the user.
 */
//============================================================================//
#ifndef Not_Configured
#define Not_Configured(MSG) \
    throw ::FET::not_configured_error(MSG, __FILE__, __LINE__)
#endif

//============================================================================//
/*!
 * \def Not_Reachable()
 *
 * Mark that this point in code cannot be reached.
 *
 * If FET_DBC_Level is non-zero, then the file and line of failure is printed.
 * Otherwise, the compiler is simply told to optimize based on the assumption
 * that it's unrechable (and if it *is* reached, undefined behavior results).
 * This should be used for code that is NOT or should not be reached instead of
 * \c assert(0), or similar.
 */
//============================================================================//
#ifndef Not_Reachable
#if FET_DBC_Level > 0
#define Not_Reachable() \
    ::FET::toss_unreachable_cookies(__FILE__, __LINE__)
#else
    #define Not_Reachable() \
    FET_UNREACHABLE
#endif
#endif


//============================================================================//
/*!
 * \def Validate(condition, message_stream)
 *
 * Throw a user-oriented verbose error when condition is not met. The
 * message_stream is passed directly into a string stream, so it can include
 * what value failed in the output. Beacuse "condition" can be a complicated
 * piece of code, we don't echo it to the user.
 */
//============================================================================//
#ifndef Validate
#define Validate(COND, MSG_STREAM) \
    do \
    { \
        if (FET_UNLIKELY(!(COND))) \
        { \
            std::ostringstream msg; \
            msg << MSG_STREAM; \
            ::FET::toss_validation_cookies(msg.str(), __FILE__, __LINE__); \
        } \
    } while (0)
#endif

//============================================================================//
// FREE NAMESPACE FUNCTIONS (for throwing assertions)
//============================================================================//
//! \brief Throw a FET::assertion for \c Require, \c Check, and \c Ensure
    FET_NORETURN
    void toss_cookies(char const * const cond,
                      char const * const file,
                      int          const line )
    {
        throw assertion( cond, file, line );
    }

//! \brief Throw a validation failure message
    FET_NORETURN
    void toss_validation_cookies(
            const std::string& msg,
            const char*        file,
            int                line)
    {
        std::ostringstream out;
        out << msg << "\n ^^^ at " << file << ":" << line;

        throw validation_error(out.str());
    }

//! \brief Throw a FET::assertion for \c Insist
    FET_NORETURN
    void toss_insist_cookies(
            const char* const cond,
            const std::string& msg,
            const char*        file,
            int                line)
    {
        std::ostringstream out;
        out << msg;
#if FET_DBC_Level > 0
        out << "\n (condition `" << cond << "` failed)\n";
#else
        // Debug mode is off, so don't trouble the user with the particulars
        FET_IGNORE(cond);
#endif
        out << "\n ^^^ at " << file << ":" << line;
        throw assertion(out.str());
    }

//! \brief Throw an assertion since this point in code should NEVER be reached
    FET_NORETURN
    void toss_unreachable_cookies(
            const char* file,
            int         line)
    {
        std::ostringstream out;
        out << "Encountered unreachable code point at " << file << ":" << line;
        throw assertion(out.str());
    }

//===========================================================================//
/*!
 * \brief Throw a FET::assertion for Require, Check, Ensure macros.
 * \return Throws an assertion.
 * \note We do not provide unit test for functions whose purpose is to throw
 * or exit.
 */
//===========================================================================//
/*      void
      toss_cookies(char const * const cond,
                   char const * const file,
                   int  const line )
      {
          throw assertion( cond, file, line );
      }
*/

//===========================================================================//
/*!
 * \brief Throw a FET::assertion for Validate macros.
 * \return Throws an assertion.
 * \note We do not provide unit test for functions whose purpose is to throw
 * or exit.
 */
//===========================================================================//
/*    void toss_validation_cookies(
            const std::string& msg,
            const char*        file,
            int                line)
    {
        std::ostringstream out;
        out << msg << "\n ^^^ at " << file << ":" << line;

        throw validation_error(out.str());
    }
*/

//===========================================================================//
/*!
 * \brief Throw a FET::assertion for Insist macros.
 */
//===========================================================================//
/*    void toss_insist_cookies(
            const char* const  cond,
            const std::string& msg,
            const char* const  file,
            const int          line)
    {
        std::ostringstream out;
        out << msg;
#if FET_DBC_Level > 0
        out << "\n (condition `" << cond << "` failed)\n";
#else
        // Debug mode is off, so don't trouble the user with the particulars
        FET_IGNORE(cond);
#endif
        out << "\n ^^^ at " << file << ":" << line;
        throw assertion(out.str());
    }
*/

//===========================================================================//
/*!
 * \brief Throw a FET::assertion for Not_Reachable macros.
 */
//===========================================================================//
/*    void toss_unreachable_cookies(
            const char* const  file,
            const int          line)
    {
        std::ostringstream out;
        out << "Encountered unreachable code point at " << file << ":" << line;
        throw assertion(out.str());
    }
*/

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_harness_FET_DBC_hh
//---------------------------------------------------------------------------//
// end of src/harness/FET_DBC.hh
//---------------------------------------------------------------------------//
