//---------------------------------------------------------------------------//
/*!
 * \file  src/harness/test/tstFET_FET_Assertions.cc
 * \brief Test of the FET_FET_Assertions implementation
 * \note  Copyright (c) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
 */
//---------------------------------------------------------------------------//

#include <iostream>
#include <stdexcept>

#include "FET_Assertions.hh"

namespace FET
{
namespace TEST
{
    //! \brief Tests that \c Assertion_Level is either 0 or 1
    void TEST_Assertion_Level()
    {
        if (!(Assertion_Level == 0 || Assertion_Level == 1))
        {
            throw "An invalid Assertion_Level was specified!";
        }
        return;
    }


    //! \brief Tests the base \c assertion object
    void TEST_Assertion_Base()
    {
        // Test explicit constructor
        try {
            throw assertion("Testing the assertion");
        } catch (assertion& e) {
            // Caught
        } catch (...) {
            throw std::logic_error("An invalid base assertion was found!");
        }

        // Try non-default constructor
        try {
            throw assertion("Testing the assertion again", __FILE__, __LINE__);
        } catch (assertion& e) {
            // Caught
        } catch (...) {
            throw std::logic_error("An invalid base assertion was found!");
        }
        return;
    }


    //! \brief Tests the \c not_implemented_error assertion
    void TEST_Assertion_Not_Implemented()
    {
        try {
            throw not_implemented_error("Pizza making", __FILE__, __LINE__);
        } catch (assertion& e) {
            // Caught
        } catch (...) {
            throw std::logic_error(
                    "An invalid not_implemented_error assertion was found!");
        }
        return;
    }


    //! \brief Tests the \c not_configured_error assertion
    void TEST_Assertion_Not_Configured()
    {
        try {
            throw not_configured_error("Pizza eating", __FILE__, __LINE__);
        } catch (assertion& e) {
            // Caught
        } catch (...) {
            throw std::logic_error(
                    "An invalid not_configured_error assertion was found!");
        }
        return;
    }


    //! \brief Tests the \c validation_error assertion
    void TEST_Assertion_Validation()
    {
        try {
            throw validation_error("Validation of something failed");
        } catch (assertion& e) {
            // Caught
        } catch (...) {
            throw std::logic_error(
                    "An invalid validation_error assertion was found!");
        }
        return;
    }


    //! \brief Tests the \c interrupt_error assertion
    void TEST_Assertion_Interrupt()
    {
        try {
            throw interrupt_error();
        } catch (std::runtime_error& e) {
            // Caught
        } catch (...) {
            throw std::logic_error(
                    "An invalid interrupt_error assertion was found!");
        }
        return;
    }


    //! \brief Performs all assertion test
    void TEST_FET_FET_Assertions()
    {
        TEST_Assertion_Level();
        TEST_Assertion_Base();
        TEST_Assertion_Not_Implemented();
        TEST_Assertion_Not_Configured();
        TEST_Assertion_Validation();
        TEST_Assertion_Interrupt();
    }

} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::TEST_FET_FET_Assertions();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/harness/test/tstFET_FET_Assertions.cc
//---------------------------------------------------------------------------//
