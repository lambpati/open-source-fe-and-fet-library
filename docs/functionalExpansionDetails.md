# Functional Expansion Details

The Functional Expansion (FE) implementation within the SHIFT Monte-Carlo neutronics code is detailed here.

> Code Version 0.0.0


## Implementation

Implementation of FEs within SHIFT are discussed here.

This will detail the code structure and methods currently used in calculating Functional Expansions. The relevant files *FE_Cell_Tally_Attr*, *FE_Cell_Tally*, and *FE_Tally_Result* will be examined in detail as to the manner in which they convert cell tallies into FE coefficients. Broad description of the basic construction of the underlying class structure, as well as the containing file structure of FET implementation is discussed in [ReadMe](../ReadMe.md) under the section FET Implementation and Usage and in more detail in [Functional Expansion Tally Details](functionalExpansionTallyDetails.md).

*FE_Tally_Result* is where the bulk of the FE computation occurs and can be divided into the following seven member functions.

1.  *FE_Tally_Result :: FE_Tally_Results* : The constructor for the FE_Tally_Result class. Creates an object called fe_holder that contains all of the information for the FE tallies including the cell, expansion type, number of terms, and dimensions. Memory is allocated for the tally which requires a dummy coefficient holder (p_coeff: holds coefficients during a particle’s life span), a coefficient holder (sum_coeff: holds coefficients between particles), and a flux coefficient holder (d_flux: manipulated coefficient holder to create flux coefficients).

2.  *FE_Tally_Result :: Accumulate_Particle_Coeff* : When a particle has died, the coefficient information from that particle’s dummy coefficient holder is accumulated into the coefficient holder.

3.  *FE_Tally_Result :: Legendre_Scale* : The function scales the position from reactor space to Legendre space. *Legendre_Scale* is overloaded for both the initial and final positions.

4.  *FE_Tally_Result :: Pn* : This is the algorithm to calculate each Legendre polynomial up to the specified order for the dimension being calculated. This is currently a recursive statement with no direct calculation optimizations. The algorithm does include memoization to reduce unnecessary repeated calculations.

5.  *FE_Tally_Result :: Legendre_Path_Length* : This uses the Legendre polynomials from *FE_Tally_Result::Pn* to calculate the results of the initial and final position and calculates the coefficient for a path length estimation. Currently this only performs calculations for 1-D or seperable 2-D and 3-D models.

6.  *FE_tally_Result :: Legendre_Collision* : This uses the Legendre polynomials from *FE_Tally_Result::Pn* to calculate the coefficient for a collision estimation. These estimations are implemented for convolved dimensions. Currently, it cannot do deconvolved estimators for higher dimensions.

7.  *FE_Tally_Result :: finalize* :  This uses the coefficient matrix created during the previous functions to perform calculations to produce the flux coefficient matrix. The uncertainty in the flux is also calculated. Future optimization could include calculating volumes and performing the necessary scaling out of Legendre space before writing to the HDF5 file.

Of these, *FE_Tally_Result :: Legendre_Collision*, *FE_Tally_Result :: Legendre_Path_Length*, and *FE_Tally_Result :: finalize* differ depending on the dimension and estimator type. *FE_Tally_Result :: Pn* will also be discussed briefly as generating appropriate Legendre polynomials is integral for any coefficient calculations.

For clarity, the FETs that are currently enabled in SHIFT are setup as such:

![](../images/FETsEnabledinShift.jpg)

## *FE_Tally_Result :: Pn*

The manner in which a Legendre polynomial of given order *n* is calculated can be divided into two strategies. The first is to directly calculate the needed order. The following are the first handful of Legendre polynomial orders directly calculated.

P<sub>0</sub>(x) = 1

P<sub>1</sub>(x) = x

P<sub>2</sub>(x) = (1/2)(3x<sup>2</sup> - 1)

P<sub>3</sub>(x) = (1/2)(5x<sup>3</sup> - 3x)

P<sub>4</sub>(x) = (1/8)(35x<sup>4</sup> - 30x<sup>2</sup> + 3)

P<sub>5</sub>(x) = (1/8)(63x<sup>5</sup> - 70x<sup>3</sup> + 15x)

P<sub>6</sub>(x) = (1/16)(231x<sup>6</sup> - 315x<sup>4</sup> + 105x<sup>2</sup> - 5)

P<sub>7</sub>(x) = (1/16)(429x<sup>7</sup> - 693x<sup>5</sup> + 315x<sup>3</sup> - 35x)

P<sub>8</sub>(x) = (1/128)(6435x<sup>8</sup> - 12012x<sup>6</sup> + 6930x<sup>4</sup> - 1260x<sup>2</sup> + 35)

...

As is apparent, as the polynomial order increases, the complexity of the calculation also increases. Each higher order, when directly calculated, takes more time than the last. Calculating them in this manner also has the limitation that the order can only be what has been directly calculated in code. If a higher order than what exists is desired, it cannot be done. There also exists a recurrence relationship for Legendre polynomials given as the following.

P<sub>0</sub>(x) = 1

P<sub>1</sub>(x) = x

P<sub>n</sub>(x) = ( ((2n - 1) * x * P<sub>n-1</sub>(x)) - ((n - 1) * P<sub>n-2</sub>(x)) ) / n

While the recurrence relationship may appear to be complex, it has some significant advantages over direct calculation. First and foremost, calculating Legendre polynomials recursively allows for any order to be specified with no limits, other than computational time. Second, and not as apparent, is that it can provide increased speed of calculation when certain conditions apply. If all previous orders up to the given maximum order are saved, in a process called memoization, they can be drawn for the new calculation without having to recalculate them again. For low order polynomials, using solely the recursion relation does not provide any speed increases. But because the recursion relation is a fixed cost for any order, there will naturally be an inflection point where the recursive strategy begins to overtake direct calculations in speed. This appears to occur around polynomials of order 10 to 12.

Merging both strategies and leveraging memoization, the best of both worlds can be achieved. For any given order *n*, if *n* is less than 10, all the polynomials will calculated directly. If *n* is greater than 10, all the polynomials up to order 10 will be calculated directly and any that extend above will be calculated recursively, thus achieving near maximum performance for the algorithm.

## *FE_Tally_Result :: Legendre_Scale*
First, a scaled position vector, called *scaled_pos*, is instantiated. To transform the position into Legendre space, the equation is provided below.

![](../images/TransformationToLegendreSpace.jpg)

*Legendre_Scale* has two overloaded functions used for *Legendre_Path_Length* and *Legendre_Collision*. The difference in the two functions is how the the *x*, the position in reactor space, is calcuated. For an explanation of how the position in reactor space is calculated, see the *Legendre_Path_Length* and *Legendre_Collision* sections.

*x<sub>min</sub>* is the minimum boundary for that position and *x<sub>max</sub>* is the maximum boundary for that position. *x<sub>min</sub>* is stored as fe_holder[tally][5+2\*iter] where 5, 7, 9 are the x, y, and z minimums for a given tally. Similarly, *x<sub>max</sub>* is stored as (fe_holder[tally][6+2\*iter] where 6, 8, 10 are the x, y, and z maximums for a tally.

## *FE_Tally_Result :: Legendre_Path_Length*

This begins equally for all variations of dimension. Vectors, *pos_init* and *pos_final*, are created to hold the initial and final position for the current particle. The initial position vector will be used later as we loop over the pathlength. A vector matrix, *b_temp*, is then created to accommodate 3-D geometry.

*pos_init* and *pos_final* are then passed through *FE_Tally_Result :: Legendre_Scale* to scale them down to Legendre space, [-1,1]. In *Legendre_Length*, *x*, the position in reactor space, is calculated by using the position of the particle, denoted as p.pos()[iter].

The 1D Path Length Estimator formula is provided below. *w<sub>i,c</sub>* and *d<sub>i,c</sub>* are the sum of the distance traveled between events where *d<sub>i,c</sub>* the path length generated by particle *i* in partition p as it travels from event *c* to event *c+1*. *x<sub>i,c</sub>* is the initial location event, and *x<sub>i,c+1</sub>* is the next location event. *ψ(x)* is the complete set of basis functions which are orthogonal to *ρ(x)*, the weight function.

![](../images/PathLengthEstimator.jpg)

This formula translates to a set of nested for loops. The first outer loop, iterates over the dimension the particle is in (e.g. x,y,z or r,z, etc) and is denoted as "*position*". The second inner loop iterates of the number of coefficients per dimension and is denoted as "*term*".

Within the second loop, there exists three conditions to separate the possible movements of the particle.

The first if statement handles the special case if the particle has not moved in the direction of the dimension we are currently iterating over. This refers to the case when x<sub>i,c</sub> = x<sub>i,c+1</sub>. In this case, the coefficient is calculated with the formula:

b_temp[position][term] = ( p.wt() \* step \* P_n_init[term] );

where *p.wt*() is the weight of the particle at the current position in the pathlength (*ρ(x)* in the formula), *step* is the distance moved by the particle from its initial position to the final position(*w<sub>i,c</sub>* \* *d<sub>i,c</sub>*), and *P_n_init[term]*, which is *ψ(x)*, is the evaluation of the Legendre polynomial for *pos_init* in the current dimension(*x<sub>i,c</sub>*).

The second if statement handles the special case where we are at the zeroth term, or the very first coefficient for the dimension we are currently iterating over. In this case, the coefficient is calculated with the formula:

b_temp[position][term] = ( p.wt() \* step \* (1.0/(pos_final[position]-pos_init[position]))
    \* (P_n_final[term+1] - P_n_init[term+1]) );

where *P_n_final[term]* is the evaluation of the Legendre polynomial for *pos_final* in the current dimension and all other terms are as described previously(*x<sub>i,c+1</sub>*). So, pos_final[position]-pos_init[position] is *x<sub>i,c+1</sub>* - *x<sub>i,c</sub>*, respectively. P_n_final[term+1] - P_n_init[term+1] is set of basis functions, *ψ(x)*.

Finally, if neither of the previous special cases apply, the coefficient is calculated based on the previous and next Legendre polynomial with the formula:

b_temp[position][term] = ( p.wt() \* step \* (1.0 / (2.0 \* term + 1.0))
    \* (1.0 / (pos_final[position] - pos_init[position]))
    \* (P_n_final[term+1] - P_n_init[term+1]
    - P_n_final[term-1] + P_n_init[term-1]) );

where all terms are as described prevously and P_n_final[term+1] - P_n_init[term+1]- P_n_final[term-1] + P_n_init[term-1] is set of basis functions, *ψ(x)*.

Over the course of iterating over the loops, each of the calculated coefficients are stored in *b_temp* and then added to the coefficient matrix, *p_coeff*, that describes the life of the particle. After *b_temp* is added to *p_coeff*, the loop repeats for the next coefficient in the dimension and when no more exist, it moves on to the next dimension.

## *FE_Tally_Result :: Legendre_Collision*
A vector, called *pos_final*, is created to hold the final position of the current particle. The final particle position is scaled from reactor space to Legendre space with boundaries [-1,1] by calling the *FE_Tally_Result::Legendre_Scale*.

The tally number denoted as *tally*, the particle called *p*, and the *step*, which is the distance moved by the particle to reach its final position, are passed into the *Legendre_Scale* function. In *Legendre_Collison*, *x*, the position in reactor space, calculated as p.pos()[iter]+step\*p.dir()[iter], which gets the position of the particle and adds the direction of the particle multiplied by the step. This calculation is performed to scale the final position of the particle, which requires the length to be moved.

Then, these positions are plugged into Legendre polynomial, *P<sub>n</sub>*, and solved.

The Collision Estimator formula is provided below. *A* is the FET coefficient; *I*, *J*, and *K* are the order of the Legendre expansion in x, y, and z respectively, and *P(n)x* is the Legendre polynomial for x, y, and z scaled from [-1, 1].

![](../images/CollisionEstimator.jpg)

A variable, *wt_sig_t* is the particle weight divided by the tally cache at a certain index of a particle. *wt_sig_t* is *A*, the FET coefficient, in the formula.

Next, a set of nested for loops are used to iterate over the x, y, and z orders for Legendre polynomials (*P(n)x*), which is denoted by fe_holder[tally][2] as x order (also as *I* in the formula), fe_holder[tally][3] as y order (also as *J*), and fe_holder[tally][4] as z order (also as *K*).

If the tally number is in one dimension for either path length or collision, a set of if, else if statements are checked to estimate the coefficients in one dimension. If the particle is zero order for all dimensions, add the *wt_sig_t* to the particle Legendre coefficients matrix, *p_coeff*, for each dimension since Legendre polynomial at P<sub>n</sub>(0) is 1.  Else if two of the x, y, z orders are the zeroth order, add the *wt_sig_t* multiplied by x, y, or z order of Legendre polynomial to *p_coeff* of that dimension.

The following else if statements estimate the coefficients in two and three dimensions. If the tally number is in two dimensions, the *wt_sig_t* is multiplied by the two of the x, y, or z order of the Legendre polynomial and added to the *p_coeff*. Similarly, if the tally number is in three dimensions, the *wt_sig_t* is multiplied by all three Legendre polynomials at x, y, and z order.

## *FE_Tally_Result :: finalize*

*FE_Tally_Result :: finalize* calculates the flux for both collision and pathlength tallies in 1-D, 2-D, and 3-D. It begins by calculating a scalar value for each permutations of 2-D and 3-D tallies, namely x_y, x_z, y_z, and x_y_z, for each individual tally. The scalar values are not calculated or used for 1-D tallies. All calculations are made with the multiplication of an orthonormalization constant for each dimension, given ortho_const[dimension]. All calculations are divided by a running tally for the number of particle interactions/collisions for each tally of interest, which is called n_counter[tally]. It then goes on to loop over the x,y,z dimensions and perform the actual calculation. The general idea is that the sum of the coefficients for that tally, in the dimensions that the flux is being calculated, are multiplied together, multiplied with the orthonormalization constant for the dimensions being calculated, then all divided by the n_counter for the tally multiplied by the scalar for the tally if available.

For example, the 3-D flux calculation for pathlength tallies is given:

d_flux[tally][x][y][z] =
                       ( sum_coeff[tally][x+1][0][0] * ortho_const[x]
                       * sum_coeff[tally][0][y+1][0] * ortho_const[y]
                       * sum_coeff[tally][0][0][z+1] * ortho_const[z] )
                       / ( n_counter[tally] * scalar_x_y_z )


## API

The application programming interface (API) for the FE implementation is discussed here, including a discussion of the construction, usage, *etc.* of FEs within SHIFT.


## Questions?

Please direct all questions to the active ExaSMR team, a sub-set of the Computational Engineering And Data Science (CEADS) group at the Idaho State University (ISU), hosted by [Dr. Leslie Kerby](mailto:kerblesl@isu.edu). As of the most recent modification of this file, this team includes, but may not be limited to, the following:

+ [Dr. Leslie Kerby](mailto:kerblesl@isu.edu)
+ [Aaron Johnson](mailto:johnaaro@isu.edu)
+ [Chase Juneau](mailto:junechas@isu.edu)
+ [Emily Elzinga](mailto:elziemil@isu.edu)
+ [Kallie McLaren](mailto:mclakall@isu.edu)
+ [Patience Lamb](mailto:lambpati@isu.edu)


## Acknowledgments

This work was supported by the ExaScale Computing Project (17-SC-2-SC).

<img src="../images/logos/ecp-logo.png" width=150>

<img src="../images/logos/ISULogo.png" width=125>
<img src="../images/logos/ornlLogo-Green.png" width=100>
