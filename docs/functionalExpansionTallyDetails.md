# Functional Expansion Tally Details

The Functional Expansion Tally (FET) implementation within the SHIFT Monte-Carlo neutronics code is detailed here.

> Code Version 0.0.0


## Implementation

Implementation of FETs within SHIFT are discussed here.

*tally.py* reads the input file provided by the users. Four separate parameters are required to be read in for FETs: the tall cell(s), functional expansion type, expansion order for each dimension, and the boundary values, the max and min, for each dimension. Functional expansion type includes the basis function, estimator type, and the number of dimensions. The current FET types in Shift are shown in the table below. The expansion order provides the number of polynomials to use for each dimension. The minimum and maximum value for each dimension provides information to properly scale the values from the reactor space to Legendre space.

![](../images/FETsEnabledinShift.jpg)

*Shift_Tallies* follows after the *tally.py* file. *Shift_Tallies* first readings in the attributes stored from the previous python script and stores the values in an array with C++. Next, a FE tally needs to be built, which is referred to FE Cell Tally hereafter. This entails using the arrays of attributes and setting these to attributes defined in *FE_Cell_Tally_Attr*. Particularly, there are five different attributes: cell unions, tally name, type, order, and dimensions.

The next step is creating the Transcore attribute. Creating an attribute for the FETs from the input file allows Shift to easily access these stored attributes. These attributes include tally cell, expansion type, order, and boundary values. A vector needs to be created to hold each of the variables within Transcore. Another necessity is to write these attributes to the HDF5 file and incorporate any reactions that may be required for the tally. This will be useful in the future if FETs are used to determine other types of reaction rates besides flux.

To create the algorithm to calculate FE Cell Tallies, *FE_Cell_Tally* and *FE_Tally_Results* are needed. *FE_Cell_Tally* interacts with the Tallier function in Shift to determine when a particle has collided, died, etc. *FE_Cell_Tally* collects the data needed to calculate the FE coefficients and then passes the data to *FE_Tally_Results*. *FE_Tally_Results* contains the algorithm to calculate the FE coefficients. It is currently broken up into seven major sections describe here for clarity.


1.  *FE_Tally_Result :: FE_Tally_Results*: An object called fe_holder is created within the constructor. fe_holder contains all of the information for the FE tallies including the cell, expansion type, number of terms, and dimensions. Memory is allocated for the tally which requires a dummy coefficient holder (p_coeff: holds coefficients during a particle’s life span), a coefficient holder (sum_coeff: holds coefficients between particles), and a flux coefficient holder (d_flux: manipulated coefficient holder to create flux coefficients).

2.  *FE_Tally_Result :: Accumulate_Particle_Coeff*: When the particle has died, the coefficient information from that particle’s dummy coefficient holder is accumulated into the coefficient holder.

3.  *FE_Tally_Result :: finalize*: This part uses the coefficient matrix to perform calculations to produce the flux coefficient matrix. The uncertainty in the flux is also calculated. Future optimization could include calculating volumes and performing the necessary scaling before writing to the HDF5 file.

4.  *FE_Tally_Result :: Pn*: The algorithm to calculate each Legendre polynomial is in *Pn*. This is currently a recursive statement but could be replaced with a function that saves previous values in order to save time and computational usage.

5.  *FE_Tally_Result :: Legendre_Path_Length*: This section uses Pn to calculate the results of the initial and final position and calculates the coefficient for a path length estimation. Currently this part only calculates each dimension in 1D.

6.  *FE_tally_Result :: Legendre_Collision*: Using the final position results from Pn, *Legendre_Collision* calculates the coefficient for a collision estimation and performs coupled estimators. Currently, it cannot do deconvolved estimators for higher dimensions.

7.  *FE_Tally_Result :: Legendre_Scale*: The function scales the position from reactor space to Legendre space. *Legendre_Scale* is overloaded for both the initial and final positions.

Once the coefficient matrix is obtained from *FE_Tally_Result*, it can then be inputted into the *Tally_Writer* function. The *Tally_Writer* instructs Shift to write the attributes and the flux results to the HDF5 file. For further improvement, the code could be optimized to easily read the flux data by breaking it up into tally and then further by dimension. However, currently the flux is outputted into one column.

Once the *Tally_Writer* function is finished, the plan is to return to Omnibus and finish with *Shift_Tallies*.

## API

The application programming interface (API) for the FET implementation is discussed here, including a discussion of the construction, usage, *etc.* of FEs within SHIFT.


## Questions?

Please direct all questions to the active ExaSMR team, a sub-set of the Computational Engineering And Data Science (CEADS) group at the Idaho State University (ISU), hosted by [Dr. Leslie Kerby](mailto:kerblesl@isu.edu). As of the most recent modification of this file, this team includes, but may not be limited to, the following:

+ [Dr. Leslie Kerby](mailto:kerblesl@isu.edu)
+ [Aaron Johnson](mailto:johnaaro@isu.edu)
+ [Chase Juneau](mailto:junechas@isu.edu)
+ [Emily Elzinga](mailto:elziemil@isu.edu)
+ [Kallie McLaren](mailto:mclakall@isu.edu)
+ [Patience Lamb](mailto:lambpati@isu.edu)


## Acknowledgments

This work was supported by the ExaScale Computing Project (17-SC-2-SC).

<img src="../images/logos/ecp-logo.png" width=150>

<img src="../images/logos/ISULogo.png" width=125>
<img src="../images/logos/ornlLogo-Green.png" width=100>
