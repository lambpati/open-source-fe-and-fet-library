# SHIFT/SCALE HB Robinson Examples

Models of HB Robinson fuelpins within a SHIFT/SCALE implementation using FET tallies are provided here. These files are structured according to the Omnibus input file documentation using a SCALE geometry model in CSAS5.

## SHIFT Input Files

To exhibit the difference between SHIFT mesh tally results and SHIFT FET results, two separate input files were created. The flux values are stored in an HDF5 format. [HB_Robinson.omn](HB_Robinson.omn) is an input file using a Separable 3D Legendre FET with bounds slightly larger than the fuelpin's diameter. [HB_Robinson_Mesh.omn](HB_Robinson_Mesh.omn) is an input file for a mesh tally with the same bounds and a fine resolution. The files can be run from Omnibus with the following command:

```shell
omnibus [filename.omn]
```

## Geometry Files

Both SHIFT input files' geometries are defined by [HB_Robinson_Core.inp](geometry/HB_Robinson_Core.inp) in SCALE CSAS5 using a 238 cross section library and an infinite reflector surrounding the fuelpin. [HB_Robinson_Core.gridFluxes.3dmap](geometry/HB_Robinson_Core.gridFluxes.3dmap) contains a 3dmap of the geometry and mesh boundaries and CSAS5 fluxes to compare between Shift and SCALE flux values.

## Questions?


Please direct all questions to the active ExaSMR team, a sub-set of the Computational Engineering And Data Science (CEADS) group at the Idaho State University (ISU), hosted by [Dr. Leslie Kerby](mailto:kerblesl@isu.edu). As of the most recent modification of this file, this team includes, but may not be limited to, the following:


Active developers:
+ [Dr. Leslie Kerby](mailto:kerblesl@isu.edu)
+ [Aaron Johnson](mailto:johnaaro@isu.edu)
+ [Chase Juneau](mailto:junechas@isu.edu)
+ [Emily Elzinga](mailto:elziemil@isu.edu)
+ [Kallie McLaren](mailto:mclakall@isu.edu)
+ [Patience Lamb](mailto:lambpati@isu.edu)

Previous developers:
+ [Katherine Wilsdon](mailto:wilskat7@isu.edu)
+ [Steven Wacker](mailto:wackstev@isu.edu)
+ [Ryan Stewart](mailto:stewryan@isu.edu)


## Acknowledgments

This work was supported by the ExaScale Computing Project (17-SC-2-SC).

<img src="../images/logos/ecp-logo.png" width=150>

<img src="../images/logos/ISULogo.png" width=125>
<img src="../images/logos/ornlLogo-Green.png" width=100>
