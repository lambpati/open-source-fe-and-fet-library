##---------------------------------------------------------------------------##
##
## .cmake/Compiler/compile_definitions.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for configuring general compiler flags for the project
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)
PreventInSourceBuild()

##---------------------------------------------------------------------------##
## Process options:
##---------------------------------------------------------------------------##
# For release builds, set FET_Assertion and FET_FET_DBC levels to 0
set(harness_defines)
if (${FET}.RELEASE)
  list(APPEND
    harness_defines
    "Assertion_Level=0"
    "FET_DBC_Level=0"
    )
else()
  list(APPEND
    harness_defines
    "Assertion_Level=1"   # Include file/line info in assertion throws
    "FET_DBC_Level=7"         # Turn ALL FET_DBC features on
    )
endif()

##---------------------------------------------------------------------------##
## Add compilation definitions for the FET library
##---------------------------------------------------------------------------##
set(${PROJECT_NAME}_compile_definitions)
list(APPEND
  ${PROJECT_NAME}_compile_definitions
  ${harness_defines}
  )

##---------------------------------------------------------------------------##
## Cache the definitions
##---------------------------------------------------------------------------##
set(
  ${PROJECT_NAME}_compile_definitions
  ${${PROJECT_NAME}_compile_definitions}
  CACHE INTERNAL
  "Global ${PROJECT_NAME} library compile definitions"
  )


##---------------------------------------------------------------------------##
##                   end of .cmake/Compiler/compile_definitions.cmake
##---------------------------------------------------------------------------##
