##---------------------------------------------------------------------------##
##
## .cmake/Compiler/cache_flags.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for caching the ${PROJECT_NAME} compilation flags (for sub-libraries)
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)
PreventInSourceBuild()

##---------------------------------------------------------------------------##
## Compound flags for both DEBUG and RELEASE builds
##---------------------------------------------------------------------------##
## For Debug
foreach(FLAG ${${PROJECT_NAME}_CXX_FLAGS})
  list(APPEND
    ${PROJECT_NAME}_CXX_FLAGS_DEBUG
    ${FLAG}
    )
endforeach()


## For Release
foreach(FLAG ${${PROJECT_NAME}_CXX_FLAGS})
  list(APPEND
    ${PROJECT_NAME}_CXX_FLAGS_RELEASE
    ${FLAG}
    )
endforeach()


##---------------------------------------------------------------------------##
## Cache flags now
##---------------------------------------------------------------------------##
## Cache general flag
set(
  ${PROJECT_NAME}_CXX_FLAGS
  "${${PROJECT_NAME}_CXX_FLAGS}"
  CACHE INTERNAL
  "General C++ compiler flags for the ${PROJECT_NAME} library"
  )


## Cache debug flags
set(
  ${PROJECT_NAME}_CXX_FLAGS_DEBUG
  "${${PROJECT_NAME}_CXX_FLAGS_DEBUG}"
  CACHE INTERNAL
  "Debug C++ compiler flags for the ${PROJECT_NAME} library"
  )


## Cache release flags
set(
  ${PROJECT_NAME}_CXX_FLAGS_RELEASE
  "${${PROJECT_NAME}_CXX_FLAGS_RELEASE}"
  CACHE INTERNAL
  "Release C++ compiler flags for the ${PROJECT_NAME} library"
  )


##---------------------------------------------------------------------------##
##                   end of .cmake/Compiiler/cache_flags.cmake
##---------------------------------------------------------------------------##
