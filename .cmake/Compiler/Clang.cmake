##---------------------------------------------------------------------------##
##
## .cmake/Compiler/Clang.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for configuring the Clang compiler flags for the project
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)
PreventInSourceBuild()

# Set default flags for both
list(APPEND
  ${PROJECT_NAME}_CXX_FLAGS
  -Wno-exceptions                 # Ignore exception errors (with noexcept)
  )


# Add flags for debug
list(APPEND
  ${PROJECT_NAME}_CXX_FLAGS_DEBUG
  "-O0"                           # NO compiler optimizations
  "-Wall"                         # Warn for all items
  "-Wextra"                       # Extra warnings (not w/ -Wall)
  "-Wconversion"                  # Warns against type conversion
  "-pedantic-errors"              # Enforces standards conformance
  "-Wno-implicit-fallthrough"     # Disable implicit fall-through warnings
  #"-Werror"                       # Make warnings errors
  )


# Add flags for release
list(APPEND
  ${PROJECT_NAME}_CXX_FLAGS_RELEASE
  )


##---------------------------------------------------------------------------##
##                   end of .cmake/Compiler/Clang.cmake
##---------------------------------------------------------------------------##
