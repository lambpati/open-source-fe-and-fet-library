##---------------------------------------------------------------------------##
##
## .cmake/Utilities/configure_utilities.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for configuring the various general macros/functions
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)

include (tools)
include (executable)
include (test)
include (library)
##---------------------------------------------------------------------------##
##                   end of .cmake/utilities/configure_utilities.cmake
##---------------------------------------------------------------------------##
