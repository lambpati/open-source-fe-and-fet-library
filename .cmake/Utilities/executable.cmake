##---------------------------------------------------------------------------##
##
## .cmake/Utilities/executable.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for executable-related macros
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)
include (tools)

##---------------------------------------------------------------------------##
## Wrapper for adding an executable
##---------------------------------------------------------------------------##
function(add_executable_wrapper NAME)

  # Parse arguments
  parse_arguments(
    exe   # Prefix
    "SOURCES;DEPENDENCIES;EXEC;INSTALL"   # List of argument types
    "BOOST;EXCLUDE_FROM_ALL"
    ${ARGN}
    )

  # Add the executable
  if (NOT exe_EXCLUDE_FROM_ALL)
    add_executable(
      ${NAME}
      ${exe_EXEC}
      ${exe_SOURCES}
      )
  else()
    add_executable(
      ${NAME}
      ${exe_EXEC}
      EXCLUDE_FROM_ALL
      ${exe_SOURCES}
      )
  endif()

  # Add dependencies for the target if given
  if (exe_DEPENDENCIES)
    # Append "${PROJECT_NAME}_" to dependency list
    set(MY_DEPENDENCIES "")
    foreach(dependency ${exe_DEPENDENCIES})
      list(APPEND MY_DEPENDENCIES "${PROJECT_NAME}_${dependency}")
    endforeach()
    if (${PROJECT_NAME}.Boost AND ${LIB_BOOST})
      list(APPEND MY_DEPENDENCIES ${Boost_LIBRARIES})
    endif()

    # Link the executable to the dependent libraries
    target_link_libraries(
      ${NAME}
      ${MY_DEPENDENCIES}
      )

    # Include the directories for each of the libraries it depends on
    foreach(LIB ${MY_DEPENDENCIES})
      target_include_directories(
        ${NAME}
        PRIVATE
        "${${LIB}_INCLUDE_DIR}"
        )
    endforeach()
  endif()

  target_compile_features(
    ${NAME}
    PUBLIC
    cxx_std_11
    # cxx_std_14
    # cxx_std_17
    # cxx_std_20
    )
  set_target_properties(
    ${NAME}
    PROPERTIES
    CXX_EXTENSIONS OFF
    )

  # Create target compiler flags now
  target_compile_options(
    ${NAME}
    PRIVATE
    # Use either Debug or Release flags
    $<$<CONFIG:Debug>:${${PROJECT_NAME}_CXX_FLAGS_DEBUG}>
    $<$<CONFIG:Release>:${${PROJECT_NAME}_CXX_FLAGS_RELEASE}>
    )

  # Now specify installation instructions:
  if (NOT exe_EXCLUDE_FROM_ALL)
    install(
      TARGETS ${NAME}
      ${exe_INSTALL}
      CONFIGURATIONS Debug Release
      )
  else()
    install(
      TARGETS ${NAME}
      ${exe_INSTALL}
      CONFIGURATIONS Debug Release
      EXCLUDE_FROM_ALL
      )
  endif()

endfunction()


##---------------------------------------------------------------------------##
##                   end of .cmake/utilities/executable.cmake
##---------------------------------------------------------------------------##
