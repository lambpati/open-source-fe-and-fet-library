##---------------------------------------------------------------------------##
##
## .cmake/FindLibrary/MPI.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for finding use including MPI
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)

if (${PROJECT_NAME}.MPI)

  # Use CMake internal to find MPI support
  find_package(MPI)

  # If auto-detection fails, try looking for local MPI installation
  # if (NOT MPI_FOUND)
  #   set(MPI_INCLUDE_DIRS "/opt/local/mpich/include" CACHE PATH "")
  #   set(MPI_LIBRARIES "/opt/local/mpich/lib/" CACHE FILEPATH))
  #   set
  # endif()

  # Include MPI if found
  if (MPI_FOUND)
    include_directoires (
      ${MPI_C_INCLUDE_DIRS}
      ${MPI_CXX_INCLUDE_DIRS}
      ${MPI_Fortran_INCLUDE_DIRS}
      )
  else()
    # Change MPI flag to OFF and warn user
    set(${PROJECT_NAME}.MPI OFF)
    message(WARNING
      "MPI support was not found"
      )
  endif()

endif()
##---------------------------------------------------------------------------##
##                   end of .cmake/FindLibrary/MPI.cmake
##---------------------------------------------------------------------------##
