##---------------------------------------------------------------------------##
##
## .cmake/Configure.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for loading all CMake modules
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)

# Append CMake module directories
list(APPEND
  CMAKE_MODULE_PATH
  "${CMAKE_SOURCE_DIR}/.cmake/General"
  "${CMAKE_SOURCE_DIR}/.cmake/FindPackage"
  "${CMAKE_SOURCE_DIR}/.cmake/FET"
  "${CMAKE_SOURCE_DIR}/.cmake/Compiler"
  "${CMAKE_SOURCE_DIR}/.cmake/Utilities"
  )

# Prevent in-source builds
include (PreventInSourceBuild)
PreventInSourceBuild()

##---------------------------------------------------------------------------##
# Configure library options
##---------------------------------------------------------------------------##
include (configure_defaults)
include (configure_utilities)
include (configure_options)
include (configure_compiler)


##---------------------------------------------------------------------------##
##                   end of .cmake/Configure.cmake
##---------------------------------------------------------------------------##
