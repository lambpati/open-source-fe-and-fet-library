##---------------------------------------------------------------------------##
##
## .cmake/FET/CTest-FETConfig.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## Library configuration test settings
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)

if (${PROJECT_NAME}.TEST)

  include (CTest)
  enable_testing()

  # Create target to build all test
  add_custom_target(
    testing
    COMMENT "${Cyan}Tests build.${ColorReset}"
    )

  # Create custom make target for building/running test
  add_custom_target(
    build-and-test
    COMMAND ${CMAKE_CTEST_COMMAND}
    COMMENT "${Cyan}Tests built.\n${Cyan}Running tests...${ColorReset}"
    )
  add_dependencies(
    build-and-test
    testing
    )

  # Message that testing is enabled
  message(STATUS
    "${PROJECT_NAME} testing is enabled")

  # Ensure build type is 'Debug' when used with testing
  if (${PROJECT_NAME}.RELEASE)
    message(STATUS
      "-- Set the option ${Red}${PROJECT_NAME}.RELEASE${ColorReset} to "
      "${Red}OFF${ColorReset} for robust testing."
      )
  endif()

  # Set some basic testing information for CDash stuff
  # set(CTEST_PROJECT_NAME "${PROJECT_NAME}")
  # set(CTEST_NIGHTLY_SART_TIME "00:00:00 MDT")
  # set(CTEST_DROP_METHOD "http")
  # set(CTEST_DROP_SITE "UNSET")
  # set(CTEST_DROP_LOCATION "UNSET")
  # set(CTEST_DROP_SITE_CDASH "UNSET")
endif()

##---------------------------------------------------------------------------##
##                   end of .cmake/FET/CTest-FETConfig.cmake
##---------------------------------------------------------------------------##
