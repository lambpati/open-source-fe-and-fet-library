##---------------------------------------------------------------------------##
##
## .cmake/FET/postprocess_options.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## Post-processes library options
##---------------------------------------------------------------------------##
## OPTIONS:
##    FET.Boost         [ON]    Compiles with Boost enabled
##    FET.BUILD_DOCS    [ON]    Creates project documentation
##    FET.[CLANG-TIDY/CPPLINT/CPPCHECK]  [ON]   Flags linter usage (DEBUG ONLY)
##    FET.MPI           [OFF]   Compile the FET library with MPI enabled
##    FET.OpenMP        [OFF]   Compile the FET library with OpenMP enabled
##    FET.PACKAGE       [OFF]   Package the FET library
##    FET.RELEASE       [OFF]   Compile the FET library as a RELEASE or DEBUG
##    FET.SHARED        [ON]    Build the FET library with shared libraries
##    FET.TEST          [ON]    Build the FET library with testing enabled
##
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)

# Build as a release
if (${PROJECT_NAME}.RELEASE)
  set(CMAKE_BUILD_TYPE "Release")
else()
  set(CMAKE_BUILD_TYPE "Debug")
endif()
message(STATUS
  "The ${PROJECT_NAME} library will be built as a "
  "${Cyan}${CMAKE_BUILD_TYPE}${ColorReset} build."
  )
include(linting)


# Find and use Boost
if (${PROJECT_NAME}.Boost)
  include (Boost)
endif()


# Find Doxygen if BUILD_DOCS is ON; modify option accordingly
if (${PROJECT_NAME}.BUILD_DOCS)
  find_package(Doxygen
    OPTIONAL_COMPONENTS dot)
  if (NOT DOXYGEN_FOUND)
    message(WARNING
      "${PROJECT_NAME} documentation could not be generated")
    set(${PROJECT_NAME}.BUILD_DOCS OFF)
  endif()

  # Check if LaTeX is available, if so turn on generation
  find_package(LATEX QUIET)
  if (LATEX_FOUND)
    set(LATEX_FOUND "YES")
  else()
    set(LATEX_FOUND "NO")
  endif()
endif()


# Find and use MPI
if (${PROJECT_NAME}.MPI)
  include (MPI)
endif()


# Find and use OpenMP
if (${PROJECT_NAME}.OpenMP)
  include (OpenMP)
endif()


# Load package configuration
include (CPack-FETConfig)


# Include library testing
include (CTest-FETConfig)


##---------------------------------------------------------------------------##
##                   end of .cmake/FET/postprocess_options.cmake
##---------------------------------------------------------------------------##
