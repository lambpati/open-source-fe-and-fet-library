##---------------------------------------------------------------------------##
##
## .cmake/FET/library_options.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for FET Software, Tools, and Documentation options
##---------------------------------------------------------------------------##
## OPTIONS:
##    FET.Boost         [ON]    Compiles with Boost enabled
##    FET.BUILD_DOCS    [ON]    Creates project documentation
##    FET.CLANG_LINTER  [ON]    Use the clang-tidy or cpplint (DEBUG only)
##    FET.MPI           [OFF]   Compile the FET library with MPI enabled
##    FET.OpenMP        [OFF]   Compile the FET library with OpenMP enabled
##    FET.PACKAGE       [OFF]   Package the FET library
##    FET.RELEASE       [OFF]   Compile the FET library as a RELEASE or DEBUG
##    FET.SHARED        [ON]    Build the FET library with shared libraries
##    FET.TEST          [ON]    Build the FET library with testing enabled
##
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)
PreventInSourceBuild()
include (add_option)


##---------------------------------------------------------------------------##
## Create FET library options
##---------------------------------------------------------------------------##
# Enables Boost usage
add_option(
  "Boost"
  "Build the ${PROJECT_NAME} with Boost enabled"
  ON)


# Library documentation
add_option(
  "BUILD_DOCS"
  "Build the ${PROJECT_NAME} library documentation"
  ON)


# MPI compilation
add_option(
  "MPI"
  "Build the ${PROJECT_NAME} library with MPI enabled"
  OFF
  )


# OpenMP compilation
add_option(
  "OpenMP"
  "Build the ${PROJECT_NAME} library with OpenMP enabled"
  OFF
  )


# Package the library
add_option(
  "PACKAGE"
  "Package the ${PROJECT_NAME} library"
  OFF
  )


# Build library as a release
add_option(
  "RELEASE"
  "Build the ${PROJECT_NAME} library as a release build"
  OFF
  )


# Build shared libraries
add_option(
  "SHARED"
  "Build the ${PROJECT_NAME} library with shared libraries"
  OFF
  )


# Include testing
add_option(
  "TEST"
  "Include testing with the build of the ${PROJECT_NAME} library"
  ON
  )


##---------------------------------------------------------------------------##
## Post-process the options
##---------------------------------------------------------------------------##
include (postprocess_options)


##---------------------------------------------------------------------------##
##                   end of .cmake/FET/library_options.cmake
##---------------------------------------------------------------------------##
