# Images

All images utilized for the documentation, mainly in the form of *ReadMe* files, is contained here.

Note that the [imageNotFound image](imageNotFound.jpg) acts as a placeholder for images that have yet to be generated.


## Questions?

Please direct all questions to the active ExaSMR team, a sub-set of the Computational Engineering And Data Science (CEADS) group at the Idaho State University (ISU), hosted by [Dr. Leslie Kerby](mailto:kerblesl@isu.edu). As of the most recent modification of this file, this team includes, but may not be limited to, the following:

Active developers:
+ [Dr. Leslie Kerby](mailto:kerblesl@isu.edu)
+ [Aaron Johnson](mailto:johnaaro@isu.edu)
+ [Chase Juneau](mailto:junechas@isu.edu)
+ [Emily Elzinga](mailto:elziemil@isu.edu)
+ [Kallie McLaren](mailto:mclakall@isu.edu)
+ [Patience Lamb](mailto:lambpati@isu.edu)

Previous developers:
+ [Katherine Wilsdon](mailto:wilskat7@isu.edu)
+ [Steven Wacker](mailto:wackstev@isu.edu)
+ [Ryan Stewart](mailto:stewryan@isu.edu)


## Acknowledgments

This work was supported by the ExaScale Computing Project (17-SC-2-SC).

<img src="logos/ecp-logo.png" width=150>

<img src="logos/ISULogo.png" width=125>
<img src="logos/ornlLogo-Green.png" width=100>
