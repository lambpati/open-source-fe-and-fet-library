get_filename_component(FET_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)

if(NOT TARGET FET::FET)
    include("${FET_CMAKE_DIR}/FETTargets.cmake")
endif()
