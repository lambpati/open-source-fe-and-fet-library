!##############################################################################
! File  : pin_FE_6order.omn
! Author: Steven Hamilton
! Date  : Wed Jul 11 2018
!##############################################################################

[PROBLEM]
name "Single pin FE tally (order 6)"
mode kcode

!################################## MODEL #####################################

[MODEL=rtk]
input "pin.rtk.xml"

!################################# PHYSICS ####################################

[PHYSICS=ce ce]
ce_lib ce_v71
mode n

!################################## COMP ######################################

[COMP]

[COMP][MATERIAL gap]
matid 0
temperature 565.0
zaid 2004
nd 1.0e-4

[COMP][MATERIAL clad]
matid 1
temperature 565.0
zaid 40090
nd 4.38e-2

[COMP][MATERIAL fuel0]
matid 2
temperature 565.0
zaid 8016 92235 92238
nd 4.5497e-02 8.2904e-04 2.1907e-02
fission true

[COMP][MATERIAL fuel1]
matid 3
temperature 565.0
zaid 8016 92235 92238
nd 4.5497e-02 8.2904e-04 2.1907e-02
fission true

[COMP][MATERIAL fuel2]
matid 4
temperature 565.0
zaid 8016 92235 92238
nd 4.5497e-02 8.2904e-04 2.1907e-02
fission true

[COMP][MATERIAL fuel3]
matid 5
temperature 565.0
zaid 8016 92235 92238
nd 4.5497e-02 8.2904e-04 2.1907e-02
fission true

[COMP][MATERIAL fuel4]
matid 6
temperature 565.0
zaid 8016 92235 92238
nd 4.5497e-02 8.2904e-04 2.1907e-02
fission true

[COMP][MATERIAL fuel5]
matid 7
temperature 565.0
zaid 8016 92235 92238
nd 4.5497e-02 8.2904e-04 2.1907e-02
fission true

[COMP][MATERIAL fuel6]
matid 8
temperature 565.0
zaid 8016 92235 92238
nd 4.5497e-02 8.2904e-04 2.1907e-02
fission true

[COMP][MATERIAL fuel7]
matid 9
temperature 565.0
zaid 8016 92235 92238
nd 4.5497e-02 8.2904e-04 2.1907e-02
fission true

[COMP][MATERIAL fuel8]
matid 10
temperature 565.0
zaid 8016 92235 92238
nd 4.5497e-02 8.2904e-04 2.1907e-02
fission true

[COMP][MATERIAL fuel9]
matid 11
temperature 565.0
zaid 8016 92235 92238
nd 4.5497e-02 8.2904e-04 2.1907e-02
fission true

[COMP][MATERIAL water0]
matid 12
temperature 565.0
zaid 1001 8016
nd 5.0e-2 2.5e-2

[COMP][MATERIAL water1]
matid 13
temperature 565.0
zaid 1001 8016
nd 5.0e-2 2.5e-2

[COMP][MATERIAL water2]
matid 14
temperature 565.0
zaid 1001 8016
nd 5.0e-2 2.5e-2

[COMP][MATERIAL water3]
matid 15
temperature 565.0
zaid 1001 8016
nd 5.0e-2 2.5e-2

[COMP][MATERIAL water4]
matid 16
temperature 565.0
zaid 1001 8016
nd 5.0e-2 2.5e-2

[COMP][MATERIAL water5]
matid 17
temperature 565.0
zaid 1001 8016
nd 5.0e-2 2.5e-2

[COMP][MATERIAL water6]
matid 18
temperature 565.0
zaid 1001 8016
nd 5.0e-2 2.5e-2

[COMP][MATERIAL water7]
matid 19
temperature 565.0
zaid 1001 8016
nd 5.0e-2 2.5e-2

[COMP][MATERIAL water8]
matid 20
temperature 565.0
zaid 1001 8016
nd 5.0e-2 2.5e-2

[COMP][MATERIAL water9]
matid 21
temperature 565.0
zaid 1001 8016
nd 5.0e-2 2.5e-2

!################################## SOURCE ####################################

[SOURCE=separable fission]

[SOURCE][SHAPE=box]
box 0.0 1.26 0.0 1.26 0.0 100.0

[SOURCE][ENERGY=watt]

!################################## TALLY #####################################

[TALLY]

![TALLY][MESH flux]
!x 0.0 1.26
!y 0.0 1.26
!z 0.0 9i 100.0
!reactions flux

[TALLY][FUNCTIONAL_EXPANSION pinFE]
union_cells 1
union_lengths 1
type 50
order 2 2 2
dimensions 0.0 1.26 0.0 1.26 0.0 100.0

!################################## SHIFT #####################################

[SHIFT]
physics ce

[SHIFT][KCODE]
npk 1e5
nk  150
nik 50
entropy_mesh manual
x_entropy 0.0 1.26
y_entropy 0.0 1.26
z_entropy 0.0 9i 100.0

!################################### POST ######################################

[RUN=mpi]
np 10

!##############################################################################
! end of pin_FE_6order.omn
!##############################################################################
