# Exnihilo Input Examples

Exnihilo input files are provided here, with an emphasis on testing the Exnihilo installation and testing the implemented Shift FET code. These files consist primarily of Omnibus input files that may be used to utilize and test features of the Shift FET code.


## Descriptions

Brief descriptions of each input file are provided here for user ease. The [fe directory](fe) contains examples utilizing the FET code. The [mesh directory](mesh) contains examples utilizing mesh tallies. Note that the [reports directory](reports) contains within it input files utilized for reports, each contained in their own directory as appropriate.


### Inputs Files

Omnibus input files are contained within the [mesh](./mesh) and [fe](./fe) directories, each containing various examples of mesh tallies and FE tallies within Shift. The available example inputs are specified below.

+ [mesh_tallies.omn](mesh/mesh_tallies.omn): Monoenergetic source in [a spherical shell](geometry/spheres.gg.omn) using one energy bin tally. Tests the `Omnibus` installation.
+ [pin_mesh_1xy.omn](mesh/pin_mesh_1xy.omn): Tests the Omnibus installation. Input describes a KCODE calculation for a watt-fission source in a [fuel pin array](geometry/pin.rtk.xml).
+ [fe_test.omn](fe/fe_test.omn): Tests the FET implementation using [a fuel pin](geometry/fuel4.inp) geometry, with a mono-direcotional, mono-energetic thermal neutron source.
+ [pin_FE_6order.omn](fe/pin_FE_6order.omn): Tests the FET implementation using a [fuel pin array](geometry/pin.rtk.xml) within a KCODE calculation.


### Geometry Files

Geometry for the associated input files may be found in the [geometry directory](./geometry).

+ [spheres.gg.omn](geometry/spheres.gg.omn): Specifies several concentric spherical material layers centered about the origin.
+ [pincell_keno.inp](geometry/pincell_keno.inp): Defines a fuel pin and all associated materials with reflected boundaries.
+ [fuel4.inp](geometry/fuel4.inp): Defines a single fuel pin from a KENO definition
+ [pin.rtk.xml](geometry/pin.rtk.xml): Defines a single RTK fuel pin array


## Questions?


Please direct all questions to the active ExaSMR team, a sub-set of the Computational Engineering And Data Science (CEADS) group at the Idaho State University (ISU), hosted by [Dr. Leslie Kerby](mailto:kerblesl@isu.edu). As of the most recent modification of this file, this team includes, but may not be limited to, the following:


Active developers:
+ [Dr. Leslie Kerby](mailto:kerblesl@isu.edu)
+ [Aaron Johnson](mailto:johnaaro@isu.edu)
+ [Chase Juneau](mailto:junechas@isu.edu)
+ [Emily Elzinga](mailto:elziemil@isu.edu)
+ [Kallie McLaren](mailto:mclakall@isu.edu)
+ [Patience Lamb](mailto:lambpati@isu.edu)

Previous developers:
+ [Katherine Wilsdon](mailto:wilskat7@isu.edu)
+ [Steven Wacker](mailto:wackstev@isu.edu)
+ [Ryan Stewart](mailto:stewryan@isu.edu)


## Acknowledgments

This work was supported by the ExaScale Computing Project (17-SC-2-SC).

<img src="../images/logos/ecp-logo.png" width=150>

<img src="../images/logos/ISULogo.png" width=125>
<img src="../images/logos/ornlLogo-Green.png" width=100>
